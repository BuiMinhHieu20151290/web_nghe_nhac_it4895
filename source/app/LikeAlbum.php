<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeAlbum extends Model
{
    //
    protected $table='like_album';
    public function album(){
        return $this->belongsTo('App\Album','id_album','id');
    }
    public function user(){
        return $this->belongsTo('App\User','id_user','id');
    }
}
