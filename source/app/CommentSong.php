<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentSong extends Model
{
    //
    protected $table='comment_song';
    public function song(){
        return $this->belongsTo('App\Song','id_song','id');
    }
    public function user(){
        return $this->belongsTo('App\User','id_user','id');
    }
}
