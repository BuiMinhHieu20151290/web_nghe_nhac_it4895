<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeSong extends Model
{
    //
    protected $table='like_song';
    public function song(){
        return $this->belongsTo('App\Song','id_song','id');
    }
    public function user(){
        return $this->belongsTo('App\User','id_user','id');
    }
}
