<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    //
    protected $table="Song";
    public function singer(){
        return $this->belongsTo('App\Singer','id_singer','id');
    }
    public function category(){
        return $this->belongsTo('App\Category','id_category','id');
    }
    public function comment_song(){
        return $this->hasMany('App\CommentSong','id_song','id');
    }
    public function like_song(){
        return $this->hasMany('App\LikeSong','id_song','id');
    }
}
