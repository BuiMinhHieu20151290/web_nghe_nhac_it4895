<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Singer extends Model
{
    //
    protected $table='singer';
    public function song(){
        return $this->hasMany('App\Song','id_singer','id');
    }
    public function album(){
        return $this->hasMany('App\Album','id_singer','id');
    }
    public function category(){
        return $this->belongsTo('App\Category','id_category','id');
    }
    public function like_singer(){
        return $this->hasMany('App\LikeSinger','id_singer','id');
    }
}
