<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailAlbum extends Model
{
    //
    protected $table='detail_album';
    public function song(){
        return $this->belongsTo('App\Song','id_song','id');
    }
    public function album(){
        return $this->belongsTo('App\Album','id_album','id');
    }
}
