<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table="category";
    public function song(){
        return $this->hasMany('App\Song','id_category','id');
    }
    public function singer(){
        return $this->hasMany('App\Singer','id_category','id');
    }
}
