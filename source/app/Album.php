<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    //
    protected $table='album';
    public function singer(){
        return $this->belongsTo('App\Singer','id_singer','id');
    }
    public function detail_album(){
        return $this->hasMany('App\DetailAlbum','id_album','id');
    }
    public function like_album(){
        return $this->hasMany('App\LikeAlbum','id_album','id');
    }
    public function comment_album(){
        return $this->hasMany('App\CommentAlbum','id_album','id');
    }
    public function category(){
        return $this->belongsTo('App\Category','id_category','id');
    }
}
