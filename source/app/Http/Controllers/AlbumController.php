<?php

namespace App\Http\Controllers;

use App\Album;
use App\Singer;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    //
    public function listAlbum(){
        $listAlbum= Album::all();
        return view('admin.page.listalbum',['listAlbum'=>$listAlbum]);
    }
    public function getAddAlbum(){
        $listSinger=Singer::all();
        return view('admin.page.addalbum',['listSinger'=>$listSinger]);
    }
    public function postAddAlbum(Request $req){
        $name=$req->name;
        $publish=$req->publish;
        $singer=$req->singer;
        $image=$req->file('image');
        $album= new Album;
        $album->name=$name;
        $album->publish=$publish;
        $album->id_singer=$singer;
        $album->image='upload/album/'.$image->getClientOriginalName();
        $album->save();
        $image->move('upload/album/',$image->getClientOriginalName());
        return redirect()->back()->with('thongbao','Thêm album thành công');
    }
    public function getUpdateAlbum($id){
        $album= Album::find($id);
        $listSinger=Singer::all();
        return view('admin.page.updatealbum',['album'=>$album,'listSinger'=>$listSinger]);
    }
    public function postUpdateAlbum(Request $req, $id){
        $album=Album::find($id);
        $name=$req->name;
        $publish=$req->publish;
        $singer=$req->singer;
        $file_image=$req->file('image');
        if($file_image!=null){
            $album->image='upload/album/'.$file_image->getClientOriginalName();
            $file_image->move('upload/album/',$file_image->getClientOriginalName());
        }
        $album->name=$name;
        $album->publish=$publish;
        $album->id_singer=$singer;
        $album->save();
        return redirect('album/');
    }
    public function getDeleteAlbum($id){
        $album=Album::find($id);
        $album->delete();
        return redirect('/album/');
    }
}
