<?php

namespace App\Http\Controllers;

use App\Album;
use App\DetailAlbum;
use App\Song;
use Illuminate\Http\Request;

class DetailAlbumController extends Controller
{
    public function getAddDetailAlbum(){
        $listAlbum= Album::all();
        $listSong= Song::all();
        return view('admin.page.adddetailalbum',['listAlbum'=>$listAlbum],['listSong'=>$listSong]);
    }
    public function postAddDetailAlbum(Request $request){
        $id_album= $request->id_album;
        $id_song=$request->id_song;
        $detail_album= new DetailAlbum;
        $detail_album->id_album= $id_album;
        $detail_album->id_song=$id_song;
        $detail_album->save();
        return redirect()->back()->with('thongbao','Thêm bài hát cho album thành công');

    }
    public function getListDetailAlbum(){
        $listDetailAlbum= DetailAlbum::all();
        return view('admin.page.listdetailalbum',['listDetailAlbum'=>$listDetailAlbum]);
    }
    public function getUpdateDetailAlbum($id){
        $detail_album= DetailAlbum::find($id);
        $listAlbum=Album::all();
        $listSong= Song::all();
        return view ('admin.page.updatedetailalbum',['detail_album'=>$detail_album,'listAlbum'=>$listAlbum,'listSong'=>$listSong]);

    }
    public function postUpdateDetailAlbum(Request $request,$id){
        $detail_album=DetailAlbum::find($id);
        $detail_album->id_album= $request->id_album;
        $detail_album->id_song= $request->id_song;
        $detail_album->save();
        return redirect('detail_album/');
    }
    public function  getDeleteDetailAlbum($id){
        $detail_album= DetailAlbum::find($id);
        $detail_album->delete();
        return redirect('detail_album/');
    }
}
