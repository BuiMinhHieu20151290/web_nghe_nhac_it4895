<?php

namespace App\Http\Controllers;

use App\CommentSong;
use App\LikeSinger;
use App\LikeSong;
use App\Song;
use Illuminate\Http\Request;
use Illuminate\Session;
class SongPageController extends Controller
{
    //
    public function index(Request $req){
        $id= $req->session()->get('id');
        $song=Song::find($id);
        $listSong=Song::all();
        $listComment=CommentSong::where('id_song',$id)->orderBy('id','DESC')->get();
        $view=view('user.page.song',['song'=>$song,'listComment'=>$listComment,'listSong'=>$listSong])->render();
        return response()->json($view);
    }
    public function comment(Request $req){
        $idUser= $req->idUser;
        $idSong= $req->idSong;
        $content= $req->content_comment;
        $comment=new CommentSong;
        $comment->id_user= $idUser;
        $comment->id_song= $idSong;
        $comment->content= $content;
        $comment->save();
        return response()->json('Thêm comment thành công');
    }
    public function loadComment(Request $req){
        $listComment= CommentSong::where('id_song',$req->id)->orderBy('id','DESC')->get();
        $view= view('user.page.comment_song',['listComment'=>$listComment])->render();
        return response()->json($view);
    }
    public function increaseViewSong(Request $req){
        $id= $req->id;
        $req->session()->put('id',$id);
        $song = Song::find($id);
        $song->view=$song->view+1;
        $song->save();
        return $song->view;
    }
    public function likeSong(Request $req){
        $idUser= $req->idUser;
        $idSong= $req->idSong;
        $likeSong= new LikeSong();
        $likeSong->id_user=$idUser;
        $likeSong->id_song= $idSong;
        $likeSong->save();
        return response()->json('Da like');
    }
    public function dislikeSong(Request $req){
        $idUser = $req->idUser;
        $idSong = $req->idSong;
        $likeSong= LikeSong::where([['id_user','=',$idUser],['id_song','=',$idSong]])->delete();
        return response()->json('Da xoa');
    }
    public function checkLike(Request $req){
        $idUser = $req->idUser;
        $idSong = $req->idSong;
        $likeSong= LikeSong::where([['id_user','=',$idUser],['id_song','=',$idSong]])->get();
        return response()->json($likeSong->count());
    }
    public function checkLikeSinger(Request $req){
        $idUser= $req->idUser;
        $idSinger= $req->idSinger;
        $like= LikeSinger::where([['id_user','=',$idUser],['id_singer','=',$idSinger]])->get();
        return response()->json($like->count());
    }
}
