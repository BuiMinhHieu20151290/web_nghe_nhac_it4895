<?php

namespace App\Http\Controllers;

use App\Category;
use App\Singer;
use Doctrine\DBAL\Event\SchemaIndexDefinitionEventArgs;
use Illuminate\Http\Request;

class SingerController extends Controller
{
    //
    public function listSinger(){
        $singers=Singer::all();
        return view('admin.page.listsinger',['listSinger'=>$singers]);
    }
    public  function getAddSinger(){
        $listCategory= Category::all();
        return view('admin.page.addsinger',['listCategory'=>$listCategory]);
    }
    public function postAddSinger(Request $req){
        $name=$req->name;
        $real_name=$req->real_name;
        $date_of_birth=$req->date_of_birth;
        $nation=$req->nation;
        $description=$req->description;
        $file_avatar=$req->file('avatar');
        $id_category= $req->id_category;
        var_dump(  $file_avatar);
        $singer =new Singer;
        $singer->name=$name;
        $singer->real_name=$real_name;
        $singer->date_of_birth=$date_of_birth;
        $singer->nation=$nation;
        $singer->description=$description;
        $singer->id_category=$id_category;
        $singer->avatar='upload/singer/'.$file_avatar->getClientOriginalName();
        $file_avatar->move('upload/singer/',$file_avatar->getClientOriginalName());
        $singer->save();
        return redirect()->back()->with('thongbao','Thêm thông tin ca sĩ thành công');
    }
    public function getUpdateSinger($id){
        $singer= Singer::find($id);
        $listCategory= Category::all();
        return view('admin.page.updatesinger',['singer'=>$singer],['listCategory'=>$listCategory]);
    }
    public function postUpdateSinger(Request $req,$id){
        $singer=Singer::find($id);
        $singer->name=$req->name;
        $singer->real_name=$req->real_name;
        $singer->date_of_birth=$req->date_of_birth;
        $singer->nation=$req->nation;
        $singer->description=$req->description;
        if($req->file('avatar')!=null){
            $singer->avatar='upload/singer/'.$req->file('avatar')->getClientOriginalName();;
        }
        $singer->id_category=$req->id_category;
        $singer->save();
        $req->file('avatar')->move('upload/singer/',$req->file('avatar')->getClientOriginalName());
        return redirect('singer/list/');
    }
    public function getDeleteSinger($id){
        $singer= Singer::find($id);
        $singer->delete();
        return redirect('singer/list');
    }
}
