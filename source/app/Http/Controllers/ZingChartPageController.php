<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ZingChartPageController extends Controller
{
    //
    public function index(){
        $view=view('user.page.zingchart')->render();
        return response()->json($view);
    }
}
