<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    //
    public function getAddUser(){
        return view('admin.page.adduser');
    }
    public function postAddUser(Request $req){
        $req->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required|min:8|max:20',
            're-password'=>'required|min:8|max:20|same:password'
        ],[
            'email.email'=>'Bạn chưa nhập đúng định dạng email',
            'password.min'=>'Password gồm tối thiểu 8 kí tự',
            'password.max'=>'Password gồm tối đa 20 kí tự',
            're-password.same'=>'Re-password cần phải giống với password'
        ]);
        $name= $req->name;
        $email=$req->email;
        $password=$req->password;
        $remember_token=Str::random(10);
        $file_avatar=$req->file('avatar');
        $user= new User;
        $user->name=$name;
        $user->email=$email;
        $user->password=Hash::make($password);
        $user->remember_token=$remember_token;
        $user->avatar= 'upload/user/'.$file_avatar->getClientOriginalName();
        $user->auth=true;
        $file_avatar->move('upload/user/',$file_avatar->getClientOriginalName());
        $user->save();
        return redirect()->back()->with('thongbao','Thêm user thành công');
    }
    public function listUser(){
        $listUser= User::all();
        return view('admin.page.listuser',['listUser'=>$listUser]);
    }
    public function getUpdateUser($id){
        $user=User::find($id);
        return view('admin.page.updateuser',['user'=>$user]);
    }
    public function getDeleteUser($id){
        $user=User::find($id);
        $user->delete();
        return redirect('users/');
    }
}
