<?php

namespace App\Http\Controllers;

use App\Album;
use App\Category;
use App\CommentAlbum;
use App\Singer;
use Illuminate\Http\Request;

class AlbumPageController extends Controller
{
    //
    public function index(){
        $listSinger= Singer::all();
        $view= view('user.page.album',['listSinger'=>$listSinger])->render();
        return response()->json($view);
    }
    public function detail(Request $req){
        $id= $req->id;
        $album= Album::find($id);
        $detail_album= Album::find($id)->detail_album;
        $list_album=Album::all();
        $view=view('user.page.detail_album',['album'=>$album,'detail_album'=>$detail_album,'list_album'=>$list_album])->render();
        return response()->json($view);
    }
    public function comment(Request $req){
        $idUser= $req->idUser;
        $idAlbum= $req->idAlbum;
        $content= $req->content_comment;
        $comment= new CommentAlbum;
        $comment->id_user=$idUser;
        $comment->id_album=$idAlbum;
        $comment->content=$content;
        $comment->save();
        return response()->json('Thêm comment thành công');
    }
    public function loadComment(Request $req){
        $id= $req->id;
        $listComment= CommentAlbum::where('id_album',$id)->orderBy('id','DESC')->get();
        $view= view('user.page.comment_album',['listComment'=>$listComment])->render();
        return response()->json($view);
    }
}
