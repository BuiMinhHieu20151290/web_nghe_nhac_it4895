<?php

namespace App\Http\Controllers;

use App\Category;
use http\Env\Response;
use Illuminate\Http\Request;

class Top100PageController extends Controller
{
    //
    public function index(){
        $listCategory= Category::all();
        $listSong= Category::find(1)->song;
        if($listSong->count()>0){
            $sortSong=$listSong->sortByDesc('view')->chunk(20);
            $view= view('user.page.top100',['listCategory'=>$listCategory,'listSong'=>$sortSong[0]])->render();
        }else{
            $view= view('user.page.top100',['listCategory'=>$listCategory,'listSong'=>null])->render();
        }
        return response()->json($view);
    }
    public function loadItem(Request $req){
        $listSong= Category::find($req->id)->song;
        if($listSong->count()>0){
            $sortSong= $listSong->sortByDesc('view')->chunk(20);
            $view=view('user.page.top100_item',['listSong'=>$sortSong[0]])->render();
            return response()->json($view);
        }else{
            return response()->json('<div style="text-align: center"><i>Danh sách trống</i></div>');
        }

    }
}
