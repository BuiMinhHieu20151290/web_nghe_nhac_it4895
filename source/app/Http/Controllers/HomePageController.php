<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Song;
use App\Album;
use App\Singer;
use Illuminate\Support\Facades\Cookie;

class HomePageController extends Controller
{
    //
    public function index(){
        $listAlbum= Album::all()->sortByDesc('id')->chunk('8');
        $listSong= Song::all()->sortByDesc('id')->chunk('12');
        $listSinger=Singer::all()->sortByDesc('like')->chunk('8');
        $listCategory= Category::all();
        $newSong=$listSong[0];
        $newAlbum=$listAlbum[0];
        $newSinger= $listSinger[0];
        return view('user.index',['newSong'=>$newSong,'newAlbum'=>$newAlbum,'listCategory'=>$listCategory,'listSinger'=>$newSinger]);
    }
    public function getHome(){
        $listAlbum= Album::all()->sortByDesc('id')->chunk('8');
        $listSong= Song::all()->sortByDesc('id')->chunk('12');
        $listSinger=Singer::all()->sortByDesc('like')->chunk('8');
        $listCategory= Category::all();
        $newSong=$listSong[0];
        $newAlbum=$listAlbum[0];
        $newSinger= $listSinger[0];
        $view=view('user.page.home',['newSong'=>$newSong,'newAlbum'=>$newAlbum,'listCategory'=>$listCategory,'listSinger'=>$newSinger])->render();
        return response()->json($view);
    }
    public function deleteCookie(){
        Cookie::unqueue('list_song');
    }
    public function getPersonalListDetail(Request $req){
        $song= $req->id;
        if(Cookie::has('lr')){
            $json = $req->cookie('lr');
            $list_music= json_decode($json);
            array_push($list_music,$song);
            $json_two= json_encode($list_music);
            Cookie::queue('lr',$json_two,'60');
        }else{
            $list_song=array();
            array_push($list_song,$song);
            $json= json_encode($list_song);
            Cookie::queue('lr',$json,'60');
        }

    }
//    public function compare($a, $array){
//        foreach ($array as $item){
//            $result= array_
//            if($item==){
//                return true;
//            }
//        }
//        return false;
//    }
    public function testCookies(Request $req){
        $value= $req->cookie('lr');
        $ar= json_decode($value);
        print_r($ar);
    }
    public function showMusicPersonal(Request $req){
        $listSong= $req->cookie('lr');
        $result= json_decode($listSong);
        $view= view('user.page.list_song_personal',['listSong'=>$result])->render();
        return response()->json($view);
    }
}
