<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CommentSong;
use App\User;
use App\Song;

class CommentSongController extends Controller
{
    //
    public function getListCommentSong(){
        $listCommentSong= CommentSong::all();
        return view('admin.page.listcommentsong',['listCommentSong'=>$listCommentSong]);

    }
    public function getViewDetailCommentSong($id){
        $commentSong= CommentSong::find($id);
        return view('admin.page.viewdetailcommentsong',['commentSong'=>$commentSong]);

    }
    public function getDeleteCommentSong($id){
        $commentSong= CommentSong::find($id);
        $commentSong->delete();
        return redirect('comment_song/');
    }
}
