<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class AuthController extends Controller
{
    //
    public function getLogin(){
        return view('user.page.login');
    }
    public function postLogin(Request $request){
        $request->validate([
            'email'=>'required',
            'password'=>'required'
        ],[
            'email.required'=>'Bạn cần nhập email',
            'password.required'=>'Bạn cần phải nhập password'
        ]);
        $checkAuth1=false;
        $checkAuth2=true;
        $auth1=array('email'=>$request->email,'password'=>$request->password,'auth'=>$checkAuth1);
        $auth2=array('email'=>$request->email,'password'=>$request->password,'auth'=>$checkAuth2);
        if(Auth::attempt($auth1)){
                return redirect('/page');
        }
        elseif (Auth::attempt($auth2)){
            return redirect('/song/list');
        }
        else{
            return redirect()->back()->with('thongbao','Đăng nhập không thành công');
        }
    }
    public function getRegister(){
        return view('user.page.register');
    }
    public function postRegister(Request $req){
        $req->validate([
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required',
            'password_confirmation'=>'required|same:password'
        ],[
            'name.required'=>'Bạn cần nhập password',
            'email.required'=>'Bạn cần nhập email',
            'password.required'=>'Bạn cần nhập password',
            'password_confirmation.same'=>'Bạn nhập mật khẩu xác nhận chưa chính xác',
            'password_confirmation.required'=>'Bạn cần nhập xác nhận mật khẩu'
        ]);
        $name=$req->name;
        $email=$req->email;
        $password=Hash::make($req->password);
        $remember_token=Str::random(10);

        $user= new User;
        $user->name=$name;
        $user->email=$email;
        $user->password=$password;
        $user->remember_token=$remember_token;
        $user->avatar="null";
        $user->auth=false;
        $user->save();
        return redirect('login');
    }
    public function demo(){
//       return response()->json("HelloWorld");
        $view =view('user.page.zingchart')->render();
        return response()->json($view);
    }
    public function logout(){
        Auth::logout();
        return redirect('/page');
    }
}
