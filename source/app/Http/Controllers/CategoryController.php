<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
class CategoryController extends Controller
{
    //
    public function listCategory(){
        $listCategory= Category::all();
        return view('admin.page.listcategory',['listCategory'=>$listCategory]);
    }
    public function getAddCategory(){
        return view('admin.page.addcategory');
    }
    public function postAddCategory(Request $req){
        $name=$req->name;
        $description=$req->description;
        $image=$req->file('image');

        $category=new Category;
        $category->name=$name;
        $category->description=$description;
        $category->image='upload/category/'.$image->getClientOriginalName();
        $category->save();
        $image->move('upload/category/',$image->getClientOriginalName());
        return redirect()->back()->with('thongbao','Thêm thể loại thành công');
    }
    public function getUpdateCategory($id){
        $category=Category::find($id);
        return view('admin.page.updatecategory',['category'=>$category]);
    }
    public function postUpdateCategory(Request $req,$id){
        $name=$req->name;
        $description=$req->description;
        $image=$req->file('image');
        $category=Category::find($id);
        $category->name=$name;
        $category->description=$description;
        if($image!=null){
            $file_name=$image->getClientOriginalName();
            $image->move('upload/category/',$file_name);
            $category->image='upload/category/'.$image->getClientOriginalName();
        }
        $category->save();
        return redirect('category/');
    }
    public function getDeleteCategory($id){
        $category=Category::find($id);
        $category->delete();
        return redirect('category/');
    }
}
