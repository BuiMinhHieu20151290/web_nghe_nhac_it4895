<?php

namespace App\Http\Controllers;

use App\Category;
use App\LikeSinger;
use App\Singer;
use Illuminate\Http\Request;

class SingerPageController extends Controller
{
    //
    public function index(){
        $listCategory=Category::all();
        $view= view('user.page.singer',['listCategory'=>$listCategory])->render();
        return response()->json($view);
    }
    public function detail(Request $request){
        $detail_singer= Singer::find($request->id);
        $singer_same=Singer::where('id_category',$detail_singer->id_category)->get();
        $view=view('user.page.detail_singer',['singer'=>$detail_singer,'singer_same'=>$singer_same])->render();
        return response()->json($view);
    }
    public function likeSinger(Request $req){
        $idUser= $req->idUser;
        $idSinger= $req->idSinger;
        $like= new LikeSinger();
        $like->id_user= $idUser;
        $like->id_singer= $idSinger;
        $like->save();
        return response()->json('da like');
    }
    public function dislikeSinger(Request $req){
        $idUser= $req->idUser;
        $idSinger= $req->idSinger;
        $likeSinge= LikeSinger::where([['id_user','=',$idUser],['id_singer','=',$idSinger]])->delete();
        return response()->json('da dislike ca si');
    }
}
