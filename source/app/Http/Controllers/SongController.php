<?php

namespace App\Http\Controllers;

use App\Category;
use App\Singer;
use App\Song;
use Illuminate\Http\Request;

class SongController extends Controller
{
    //
    public function getAddSong(){
        $listSinger= Singer::all();
        $listCategory=Category::all();
        return view('admin.page.addsong',['listCategory'=>$listCategory,'listSinger'=>$listSinger]);
    }
    public function postAddSong(Request $req){
        $name=$req->name;
        $lyric=$req->lyric;
        $id_singer=$req->id_singer;
        $composer=$req->composer;
        $id_category=$req->id_category;
        $file_mp3=$req->file('file');
        $file_image=$req->file('image');

        $song= new Song;
        $song->name=$name;
        $song->lyric=$lyric;
        $song->id_singer=$id_singer;
        $song->composer=$composer;
        $song->id_category=$id_category;
        $song->file='upload/mp3/'.$file_mp3->getClientOriginalName();
        $song->image='upload/song/'.$file_image->getClientOriginalName();
        $song->like=0;
        $song->view=0;
        $song->save();

        $file_mp3->move('upload/mp3/',$file_mp3->getClientOriginalName());
        $file_image->move('upload/song/',$file_image->getClientOriginalName());
        return redirect()->back()->with('thongbao','Thêm bài hát thành công');
    }
    public function getUpdateSong($id){
        $song=Song::find($id);
        $listSinger=Singer::all();
        $listCategory=Category::all();
        return view('admin.page.updatesong',['song'=>$song,'listSinger'=>$listSinger,'listCategory'=>$listCategory]);
    }
    public function postUpdateSong(Request $req,$id){
        $song= Song::find($id);
        $name=$req->name;
        $lyric=$req->lyric;
        $id_singer=$req->id_singer;
        $composer=$req->composer;
        $id_category=$req->id_category;
        $file_mp3=$req->file('file');
        $file_image=$req->file('image');
        if($file_mp3!=null){
            $song->file='upload/mp3/'.$file_mp3->getClientOriginalName();
            $file_mp3->move('upload/mp3/',$file_mp3->getClientOriginalName());
        }
        if($file_image!=null){
            $song->image='upload/song/'.$file_image->getClientOriginalName();
            $file_image->move('upload/song/',$file_image->getClientOriginalName());
        }
        $song->name=$name;
        $song->lyric=$lyric;
        $song->id_singer=$id_singer;
        $song->composer=$composer;
        $song->id_category=$id_category;
        $song->save();
        return redirect('song/list');
    }
    public function listSong(){
        $listSong=Song::all();
        return view('admin.page.listsong',['listSong'=>$listSong]);
    }
    public function getDeleteSong($id){
        $song= Song::find($id);
        $song->delete();
        return redirect('song/list');
    }
}
