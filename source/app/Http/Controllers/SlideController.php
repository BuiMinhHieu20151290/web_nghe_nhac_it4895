<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;

//use Doctrine\DBAL\Event\SchemaIndexDefinitionEventArgs;


class SlideController extends Controller
{
    public function listSlide()
    {
        $slides = Slide::all();
        return view('admin.page.listslide', ['listSlide' => $slides]);
    }

    public function getInsertSlide()
    {
        return view('admin.page.addslide');
    }

    public function postInsertSlide(Request $request)
    {
        $slide_name = $request->slide_name;
        $slide_description = $request->slide_description;
        $slide_file = $request->file('slide_file');
        var_dump($slide_file);
        $slide_show = $request->slide_show;
        $slide = new Slide;
        $slide->name = $slide_name;
        $slide->description = $slide_description;
        $slide->file = 'upload/slide/' . $slide_file->getClientOriginalName();
        $slide->show = $slide_show;
        $slide_file->move('upload/slide/', $slide_file->getClientOriginalName());
        $slide->save();
        return redirect()->back()->with('thongbao', 'Thêm thông tin slide thành công');
    }
    public function postUpdateSlide(Request $request,$id){
        $slide=Slide::find($id);
        $slide->name=$request->slide_name;
        $slide->description=$request->slide_description;
        if($request->file('slide_file')!=null){
            $slide->file='upload/slide/'.$request->file('slide_file')->getClientOriginalName();;
        }
        $slide->show=$request->slide_show;
        $slide->save();
        $request->file('slide_file')->move('upload/slide/',$request->file('slide_file')->getClientOriginalName());
        return redirect('slide/');
    }
    public function getUpdateSlide($id){
        $slide= Slide::find($id);
        return view('admin.page.updateslide',['slide'=>$slide]);
    }
    public function getDeleteSlide($id){
        $slide= Slide::find($id);
        $slide->delete();
        return redirect('slide/');
    }

}

