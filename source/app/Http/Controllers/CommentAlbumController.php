<?php

namespace App\Http\Controllers;
use App\CommentAlbum;

use Illuminate\Http\Request;

class CommentAlbumController extends Controller
{
    //
    public function getListCommentAlbum(){
        $listCommentAlbum= CommentAlbum::all();
        return view('admin.page.listcommentalbum',['listCommentAlbum'=>$listCommentAlbum]);

    }
    public function getViewDetailCommentAlbum($id){
        $commentAlbum= CommentAlbum::find($id);
        return view('admin.page.viewdetailcommentalbum',['commentAlbum'=>$commentAlbum]);

    }
    public function getDeleteCommentAlbum($id){
        $commentAlbum= CommentAlbum::find($id);
        $commentAlbum->delete();
        return redirect('comment_album/');
    }
}
