var audio= document.getElementById("audio");
var image= document.getElementById("imageSmall");
var img=document.getElementById("img");
audio.ontimeupdate=function(){
    action();
}
function action(){
    if(audio.paused==true){
        document.getElementById("img").style.animationPlayState="paused";
        document.getElementById("imageSmall").style.animationPlayState = "paused";
    }else{
        document.getElementById("imageSmall").style.animationPlayState = "running";
        document.getElementById("img").style.animationPlayState="running";
    }
}
action();