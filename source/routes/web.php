<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.page.quanlicasi');
});
Route::prefix('singer')->group(function (){
    Route::get('/add','SingerController@getAddSinger');
    Route::post('/add','SingerController@postAddSinger')->name('addSinger');
    Route::get('/update/{id}','SingerController@getUpdateSinger')->name('updateSinger');
    Route::post('/update/{id}','SingerController@postUpdateSinger')->name('updateSinger');
    Route::get('/list/','SingerController@listSinger');
    Route::get('/delete/{id}','SingerController@getDeleteSinger')->name('deleteSinger');
}
);
Route::prefix('album')->group(
    function (){
        Route::get('/add','AlbumController@getAddAlbum')->name('addAlbum');
        Route::post('/add','AlbumController@postAddAlbum')->name('addAlbum');
        Route::get('/update/{id}','AlbumController@getUpdateAlbum')->name('updateAlbum');
        Route::post('/update/{id}','AlbumController@postUpdateAlbum')->name('updateAlbum');
        Route::get('/delete/{id}','AlbumController@getDeleteAlbum')->name('deleteAlbum');
        Route::get('/list/','AlbumController@listAlbum');
    }
);
Route::prefix('song')->group(function (){

        Route::get('/add','SongController@getAddSong');
        Route::post('/add','SongController@postAddSong')->name('addSong');
        Route::get('/update/{id}','SongController@getUpdateSong')->name('updateSong');
        Route::post('/update/{id}','SongController@postUpdateSong')->name('updateSong');
        Route::get('/list/','SongController@listSong');
        Route::get('/delete/{id}','SongController@getDeleteSong')->name('deleteSong');
    }
);
Route::prefix('users')->group(function (){
    Route::get('/add','UserController@getAddUser');
    Route::post('/add','UserController@postAddUser')->name('addUser');
    Route::get('/update/{id}','UserController@getUpdateUser')->name('updateUser');
    Route::post('/update/{id}','UserController@postUpdateUser')->name('updateUser');
    Route::get('/delete/{id}','UserController@getDeleteUser')->name('deleteUser');
    Route::get('/','UserController@listUser');
});
Route::prefix('category')->group(function (){
    Route::get('/add','CategoryController@getAddCategory');
    Route::post('/add','CategoryController@postAddCategory')->name('addCategory');
    Route::get('/update/{id}','CategoryController@getUpdateCategory')->name('updateCategory');
    Route::post('/update/{id}','CategoryController@postUpdateCategory')->name('updateCategory');
    Route::get('/','CategoryController@listCategory');
    Route::get('/delete/{id}','CategoryController@getDeleteCategory')->name('deleteCategory');
});
Route::get('commentsong',function (){
    return view('admin.page.commentsong');
});
Route::get('commentalbum',function (){
    return view('admin.page.commentalbum');
});
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('login','AuthController@getLogin')->name('login');
Route::post('login','AuthController@postLogin')->name('login');
Route::get('/home','HomePageController@index')->name('home');
Route::get('register','AuthController@getRegister');
Route::post('register','AuthController@postRegister')->name('register');
Route::get('/logout','AuthController@logout')->name('logout');
Route::get('/page','HomePageController@index')->name('page');
Route::get('/home_zing','HomePageController@getHome');
Route::get('/zingchart','ZingChartPageController@index');
Route::get('/top100','Top100PageController@index');
Route::get('/album','AlbumPageController@index');
Route::get('/detail_Album','AlbumPageController@detail');
Route::get('/singer_user','SingerPageController@index');
Route::get('/detail_singer','SingerPageController@detail');
Route::post('/comment_album','AlbumPageController@comment');
Route::get('/loadCommentAlbum','AlbumPageController@loadComment');
Route::get('/loadDetailSong','SongPageController@index');
Route::post('/comment_song','SongPageController@comment');
Route::get('/loadCommentSong','SongPageController@loadComment');
Route::get('/increaseViewSong','SongPageController@increaseViewSong');
Route::get('/likeSong','SongPageController@likeSong');
Route::get('/dislikeSong','SongPageController@dislikeSong');
Route::get('/checkLike','SongPageController@checkLike');
Route::get('/checkLikeSinger','SongPageController@checkLikeSinger');
Route::get('/likeSinger','SingerPageController@likeSinger');
Route::get('/dislikeSinger','SingerPageController@dislikeSinger');
Route::get('/top100_item','Top100PageController@loadItem');
Route::get('/music_personal','HomePageController@getPersonalListDetail');
Route::get('/text','HomePageController@testCookies');
Route::get('/show','HomePageController@showMusicPersonal');
ROute::get('deleteCookies','HomePageController@deleteCookie');
//Route for Slide
Route::prefix('slide')->group(function(){
    Route::get('/add','SlideController@getInsertSlide');
    Route::post('/add','SlideController@postInsertSlide')->name('addSlide');
    Route::get('/','SlideController@listSlide');
    Route::get('/update/{id}','SlideController@getUpdateSlide')->name('updateSlide');
    Route::post('/update/{id}','SlideController@postUpdateSlide')->name('updateSlide');
    Route::get('/delete/{id}','SlideController@getDeleteSlide')->name('deleteSlide');
});
Route::prefix('detail_album')->group(function(){
    Route::get('/add','DetailAlbumController@getAddDetailAlbum');
    Route::post('/add','DetailAlbumController@postAddDetailAlbum')->name('addDetailAlbum');
    Route::get('/list','DetailAlbumController@getListDetailAlbum');
    Route::get('/update/{id}','DetailAlbumController@getUpdateDetailAlbum')->name('updateDetailAlbum');
    Route::post('/update/{id}','DetailAlbumController@postUpdateDetailAlbum')->name('updateDetailAlbum');
    Route::get('/delete/{id}','DetailAlbumController@getDeleteDetailAlbum')->name('deleteDetailAlbum');
});
Route:: prefix('comment_song')->group(function (){
    Route::get('/','CommentSongController@getListCommentSong');
    Route::get('/view_detail/{id}','CommentSongController@getViewDetailCommentSong')->name('viewDetailCommentSong');
    Route::get('/delete/{id}','CommentSongController@getDeleteCommentSong')->name('deleteCommentSong');
});
Route:: prefix('comment_album')->group(function (){
    Route::get('/','CommentAlbumController@getListCommentAlbum');
    Route::get('/view_detail/{id}','CommentAlbumController@getViewDetailCommentAlbum')->name('viewDetailCommentAlbum');
    Route::get('/delete/{id}','CommentAlbumController@getDeleteCommentAlbum')->name('deleteCommentAlbum');
});

