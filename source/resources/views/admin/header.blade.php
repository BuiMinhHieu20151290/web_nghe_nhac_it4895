<div class="header-content">
    <nav class="navbar navbar-expand-sm navbar-dark" style="border-bottom: 1px solid #ececec">
        <form class="form-inline" action="/action_page.php" >
            <input class="form-control mr-sm-2 " type="text" placeholder="Search" size="80">
            <button class="btn btn-success" type="submit">Search</button>
        </form>
        <div class="info d-flex align-items-center ml-auto mr-5">
            <img src="{{asset(Auth::user()->avatar)}}" width="35px" height="35px" class="rounded-circle">
            <div class="ml-4" style="font-weight: bold">
                @if(Auth::check())
                    <button type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" >
                        {{Auth::user()->name}} </button>
                    <div class="detail">

                            <a href="{{route('logout')}}" style="color: #c51f1a" class="dropdown-item">Logout</a>
                    </div>
                @endif
               {{-- <span class="glyphicon glyphicon-chevron-down"></span></div>--}}
           {{-- <div class="detail">
                <div><span class="glyphicon glyphicon-log-out text-primary mr-2"></span> Logout</div>
            </div>--}}
</nav>
</div>