<div class=" menu">
    <div class="menu-header">
        ZingMP3
    </div>
    <ul>
        <li>
            <a href="#" style="display: block">
                <span class="glyphicon glyphicon-home" ></span> Dashboard
            </a>
            <!--<ul class="submenu">-->
            <!--<li>-->
            <!--<a href="#">-->
            <!--Dasboard-->
            <!--</a>-->
            <!--</li>-->
            <!--<li>-->
            <!--<a href="#">-->
            <!--Dasboard-->
            <!--</a>-->
            <!--</li>-->
            <!--</ul>-->
        </li>
        <li>
            <a href="http://localhost:8000/song/list">
                <span class="glyphicon glyphicon-music" ></span> Quản lí bài hát
            </a>
            <ul class="submenu">
                <li>
                    <a href="http://localhost:8000/song/list">
                        Danh sách bài hát
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8000/song/add">
                        Thêm bài hát
                    </a>
                </li>

            </ul>
        </li>
        <li>
            <a href="#">
                <span class="glyphicon glyphicon-book" ></span> Quản lí album
            </a>
            <ul class="submenu">
                <li>
                    <a href="http://localhost:8000/album/list">
                        Danh sách album
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8000/album/add">
                        Thêm album
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8000/detail_album/list">
                        Chi tiết album
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8000/detail_album/add">
                        Thêm chi tiết album
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="http://localhost:8000/singer/list">
                <span class="glyphicon glyphicon-time" ></span> Quản lí ca sĩ
            </a>
            <ul class="submenu">
                <li>
                    <a href="http://localhost:8000/singer/list">
                        Danh sách ca sĩ
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8000/singer/add">
                        Thêm ca sĩ
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="http://localhost:8000/category/">
                <span class="glyphicon glyphicon-ok" ></span> Quản lí thể loại
            </a>
            <ul class="submenu">
                <li>
                    <a href="http://localhost:8000/category/">
                        Danh sách thể loại
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8000/category/add">
                        Thêm thể loại
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <span class="glyphicon glyphicon-comment" ></span> Quản lí comment
            </a>
            <ul class="submenu">
                <li>
                    <a href="http://localhost:8000/comment_song/">
                        Comment bài hát
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8000/comment_album/">
                        Comment album
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="http://localhost:8000/users/">
                <span class="glyphicon glyphicon-user" ></span> Quản lí user
            </a>
            <ul class="submenu">
                <li>
                    <a href="http://localhost:8000/users/">
                        Danh sách user
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8000/users/add">
                        Thêm admin
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <span class="glyphicon glyphicon-picture" ></span> Quản lí slide
            </a>
            <ul class="submenu">
                <li>
                    <a href="http://localhost:8000/slide/">
                        Danh sách slide
                    </a>
                </li>
                <li>
                    <a href="http://localhost:8000/slide/add">
                        Thêm slide
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>