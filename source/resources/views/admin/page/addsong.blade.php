@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('action/css/thembaihat.css')}}">
    @endsection
@section('content')
    <div class="content-add-song" style="padding: 15px;">
        <h2 style="text-align: center">Thêm thông tin bài hát</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
            @endif
        <form action="{{route('addSong')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên bài hát:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên bài hát" name="name">
            </div>
            <div class="form-group">
                <label >Lời bài hát</label>
                <textarea class="form-control" rows="5" name="lyric"></textarea>
            </div>
            <div class="form-inline form-group ">
                <label class="mr-sm-2">Ca sĩ:</label>
                <select class="form-control" name="id_singer" style="width: 285px" required>
                    <option value="">Choose...</option>
                    @foreach($listSinger as $singer)
                        <option value="{{$singer->id}}">{{$singer->name}}</option>
                        @endforeach
                </select>
                <label  class="ml-5" >Nhạc sĩ:</label>
                <input type="text" name="composer" class="form-control" style="width: 285px" required>
                <label class="ml-5 " >Thể loại:</label>
                <select class="form-control" name="id_category" style="width: 285px" required>
                    <option value="">Choose...</option>
                    @foreach($listCategory as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                </select>

            </div>
            <div class="form-group">
                <label >File:</label>
                <input type="file" class="form-control-file" required name="file" accept="audio/mp3" />
            </div>
            <div class="form-group">
                <label >Image:</label>
                <input type="file" class="form-control-file" required name="image" accept="image/*" >
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block" >Thêm bài hát</button>
            </div>
        </form>
    </div>
    @endsection