@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
    @endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Thêm thông tin thể loại</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
            @endif
        <form action="{{route('addCategory')}}" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-group">
                <label >Tên thể loại:</label>
                <input type="text" class="form-control" placeholder="Nhập tên thể loại" name="name" required>
            </div>
            <div class="form-group">
                <label >Miêu tả</label>
                <textarea class="form-control" rows="3"  required name="description"></textarea>
            </div>
            <div class="form-group">
                <label for="">Image</label>
                <input type="file" name="image" class="form-control-file" accept="image/*" required/>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block" >Thêm thể loại</button>
            </div>
        </form>
    </div>
    @endsection