@extends('admin.index')
@section('content')
    <div class="content-add-song" style="padding: 15px;">
        <h2 style="text-align: center">Sửa thông tin người dùng</h2>
        <form action="{{route('updateUser',['id'=>$user->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên người dùng:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên người dùng" name="name" value="{{$user->name}}">
            </div>
            <div class="form-group">
                <label>Email:</label>
                <input type="email" class="form-control" placeholder="Nhập email người dùng" name="email " value="{{$user->email}}">
            </div>
            <div class="form-group">
                <label>Password:</label>
                <input type="password" class="form-control" placeholder="Nhập password" name="password" value="{{$user->password}}">
            </div>
            <div class="form-group">
                <label>Re-Password:</label>
                <input type="password" class="form-control"  placeholder="Nhập email" name="re-password" value="{{$user->password}}">
            </div>
            <div class="form-group">
                <label>Avatar</label>
                <div>
                    {{$user->avatar}}
                </div>
                <input type="file" class="form-control-file" name="avatar" accept="image/*">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block" >Update thông tin user</button>
            </div>
        </form>
    </div>
@endsection