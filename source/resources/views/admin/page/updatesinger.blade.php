@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
@endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Update thông tin ca sĩ</h2>
        <form action="{{route('updateSinger',['id'=>$singer->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên ca sĩ:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên ca sĩ" name="name" value="{{$singer->name}}" required>
                <div class="valid-feedback">
                    Looks good!
                </div>

            </div>
            <div class="form-group">
                <label >Tên thật:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên thật ca sĩ" name="real_name" value="{{$singer->real_name}}" required>
            </div>
            <div class="form-group">
                <label >Ngày sinh:</label>
                <input type="date" class="form-control"  placeholder="Nhập ngày sinh ca sĩ" name="date_of_birth" value="{{$singer->date_of_birth}}" required>
            </div>
            <div class="form-group">
                <label >Quốc tịch</label>
                <input type="text" class="form-control"  placeholder="Nhập quốc tịch" name="nation" value="{{$singer->nation}}" required>
            </div>
            <div class="form-group">
                <label >Thông tin</label>
                <textarea class="form-control" rows="3" name="description"  required>{{$singer->description}}</textarea>
            </div>
            <div class="form-group">
                <label>Avatar</label>
                {{--<input type="text" name="avatar" value="{{$singer->avatar}}" disabled>--}}
                <input type="file" class="form-control-file" name="avatar" accept="image/*" >
            </div>
            <div class="form-group">
                <label>Category</label>
                <select name="id_category" class="form-control" required>
                    <option value="">Choose...</option>
                    @foreach($listCategory as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Update thông tin ca sĩ</button>
            </div>
        </form>
    </div>
@endsection