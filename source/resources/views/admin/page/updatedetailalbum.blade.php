@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
@endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Update thông tin chi tiết album</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('updateDetailAlbum',['id'=>$detail_album->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Tên Album:</label>
                <select name="id_album" class="form-control" required>
                    @foreach($listAlbum as $album)
                        @if($album->id==$detail_album->album->id)
                            <option value="{{$album->id}}" selected>{{$album->name}}</option>
                        @else
                            <option value="{{$album->id}}">{{$album->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Tên bài hát:</label>
                <select name="id_song" class="form-control" required>
                    <option value="1">Chạm khẽ tim anh 1 chút thôi!</option>
                    @foreach($listSong as $song)
                        @if($song->id==$detail_album->song->id)
                            <option value="{{$song->id}}" selected>{{$song->name}}</option>
                        @else
                            <option value="{{$song->id}}">{{$song->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Update bài hát cho album</button>
            </div>
        </form>
    </div>
@endsection