@extends('admin.index');
@section('css')
    <link rel="stylesheet" href="{{asset('action/css/thembaihat.css')}}">
    @endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Thêm thông tin slide</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success"> {{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('addSlide')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên slide:</label>
                <input class="form-control" placeholder="Nhập tên slide" name="slide_name" required>
            </div>
            <div class="form-group">
                <label >Mô tả slide</label>
                <textarea class="form-control" rows="5" name="slide_description" required></textarea>

            </div>
            <div class="form-group">
                <label for="exampleFormControlFile1">Chọn file</label>
                <input type="file" class="form-control-file" name="slide_file" required>
            </div>
            <div class="form-group">
                <label >Show</label>
                <input class="form-control" rows="5" name="slide_show" required>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary" style="width: 150px; font-weight: bold">Thêm slide</button>
            </div>
        </form>
    </div>
    @endsection