@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlibaihat.css')}}">
    @endsection
@section('content')
    <div id="quanlibaihat" style="padding: 15px;" >
        <table class="table table-striped" id="danhsachbaihat" >
            <thead class="thead-dark">
            <tr style="text-align: center">
                <th  rowspan="2">STT</th>
                <th scope="col" rowspan="2">Người dùng</th>
                <th scope="col" rowspan="2">Bài hát</th>
                <th scope="col" rowspan="2">Nội dung</th>
                <th scope="col" rowspan="2">Ngày đăng</th>
                <th scope="col" colspan="2" style="text-align: center" rowspan="1" >Actions</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center" >Detail</th>
                <th >Delete</th>
            </tr>
            </thead>
            <tbody>
                @foreach($listCommentSong as $commentSong)
                    <tr>
                        <td>{{$commentSong->id}}</td>
                        <td>{{$commentSong->user->name}}</td>
                        <td>{{$commentSong->song->name}}</td>
                        <td>{{$commentSong->content}}</td>
                        <td>{{$commentSong->created_at}}</td>

                        <td>
                            <a href="{{route('viewDetailCommentSong',['id'=>$commentSong->id])}}" type="button" class="btn btn-primary " >Xem chi tiết</a>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#myModal'.$commentSong->id}}" >
                                Xóa
                            </button>
                            <div class="modal" id="{{'myModal'.$commentSong->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Thông báo</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            Bạn có chắc chắn muốn xóa không
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            <a href="{{route('deleteCommentSong',['id'=>$commentSong->id])}}" class="btn btn-primary">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
            </tbody>
        </table>

    </div>
    @endsection
