@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlibaihat.css')}}">
    @endsection
@section('content')
    <div id="quanlibaihat" style="padding: 15px;" >
        <table class="table table-striped" id="danhsachbaihat" >
            <thead class="thead-dark">
            <tr style="text-align: center">
                <th  rowspan="2">STT</th>
                <th scope="col" rowspan="2">Name</th>
                <th scope="col" rowspan="2">Publish</th>
                <th scope="col" rowspan="2">Singer</th>
                <th scope="col" rowspan="2">Image</th>
                <th scope="col" colspan="2" style="text-align: center" rowspan="1" >Actions</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center" >Delete</th>
                <th >Update</th>
            </tr>
            </thead>
            <tbody>
                @foreach($listAlbum as $key=>$album)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$album->name}}</td>
                        <td>{{$album->publish}}</td>
                        <td>{{$album->singer->name}}</td>
                        <td>
                            <img src="{{asset($album->image)}}" width="40px" height="40px">
                        </td>
                        <td>
                            <a href="{{route('updateAlbum',['id'=>$album->id])}}" type="button" class="btn btn-primary " >Sửa</a>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#myModal'.$album->id}}" >
                                Xóa
                            </button>
                            <div class="modal" id="{{'myModal'.$album->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Thông báo</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            Bạn có chắc chắn muốn xóa không
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            <a href="{{route('deleteAlbum',['id'=>$album->id])}}" class="btn btn-primary">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
            </tbody>
        </table>

    </div>
    @endsection
