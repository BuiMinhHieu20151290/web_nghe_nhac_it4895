@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
@endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Thêm thông tin chi tiết album</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('addDetailAlbum')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên Album:</label>
                <select name="id_album" class="form-control" required>
                    <option value="">Choose...</option>
                    @foreach($listAlbum as $album)
                        <option value="{{$album->id}}">{{$album->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label >Tên bài hát:</label>
                <select name="id_song" class="form-control" required>
                    <option value="">Choose...</option>
                    {{--<option value="1">Chạm khẽ tim anh 1 chút thôi!</option>--}}
                    @foreach($listSong as $song)
                        <option value="{{$song->id}}">{{$song->name}}</option>
                    @endforeach*
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Thêm bài hát cho album</button>
            </div>
        </form>
    </div>
@endsection