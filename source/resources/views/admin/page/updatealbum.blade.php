@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
@endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Update album</h2>
        <form action="{{route('updateAlbum',['id'=>$album->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên album:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên album" name="name" required value="{{$album->name}}">
            </div>
            <div class="form-group">
                <label >Ngày phát hành</label>
                <input type="date" class="form-control" name="publish" required value="{{$album->publish}}">
            </div>
            <div class="form-group">
                <label>Ca sĩ:</label>
                <select class="custom-select mr-sm-2" required name="singer">
                    @foreach($listSinger as $singer)
                        @if($singer->id==$album->singer->id)
                            <option value="{{$singer->id}}" selected>{{$singer->name}}</option>
                            @else
                            <option value="{{$singer->id}}">{{$singer->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Image</label>
                <div >
                    <input type="text" value="{{$album->image}}" disabled>
                </div>
                <input type="file" class="form-control-file" required name="image" accept="image/*">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block" >Update album</button>
            </div>
        </form>
    </div>
@endsection
