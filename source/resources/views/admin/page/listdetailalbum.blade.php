@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlibaihat.css')}}">
@endsection
@section('content')
    <div id="quanlibaihat" style="padding: 15px;" >
        <table class="table table-striped" id="danhsachbaihat" >
            <thead class="thead-dark">
            <tr style="text-align: center">
                <th  rowspan="2">STT</th>
                <th scope="col" rowspan="2" >Album</th>
                <th scope="col" rowspan="2" >Bài hát</th>
                <th scope="col" colspan="2" style="text-align: center" rowspan="1" >Actions</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center" >Delete</th>
                <th >Update</th>
            </tr>
            </thead>
            <tbody>
                @foreach($listDetailAlbum as $key=>$detail_album)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$detail_album->album->name}}</td>
                        <td>{{$detail_album->song->name}}</td>
                        <td>
                            <a type="button" class="btn btn-primary " onclick="table_hidden()" href="{{route('updateDetailAlbum',['id'=>$detail_album->id])}}" >Sửa</a>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#myModal'.$detail_album->id}}" >
                                Xóa
                            </button>
                            <div class="modal" id="{{'myModal'.$detail_album->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Thông báo</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            Bạn có chắc chắn muốn xóa không
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            <a href="{{route('deleteDetailAlbum',['id'=>$detail_album->id])}}" class="btn btn-primary">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
        {{--<div class="container">--}}
            {{--<h2>Modal Example</h2>--}}
            {{--<!-- Button to Open the Modal -->--}}
            {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">--}}
                {{--Open modal--}}
            {{--</button>--}}

            {{--<!-- The Modal -->--}}
            {{--<div class="modal" id="myModal">--}}
                {{--<div class="modal-dialog">--}}
                    {{--<div class="modal-content">--}}

                        {{--<!-- Modal Header -->--}}
                        {{--<div class="modal-header">--}}
                            {{--<h4 class="modal-title">Thông báo</h4>--}}
                            {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                        {{--</div>--}}

                        {{--<!-- Modal body -->--}}
                        {{--<div class="modal-body">--}}
                            {{--Bạn có chắc chắn muốn xóa không--}}
                        {{--</div>--}}

                        {{--<!-- Modal footer -->--}}
                        {{--<div class="modal-footer">--}}
                            {{--<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>--}}
                            {{--<a href="{{route('')}}" class="btn btn-primary">Delete</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}
    </div>
@endsection
