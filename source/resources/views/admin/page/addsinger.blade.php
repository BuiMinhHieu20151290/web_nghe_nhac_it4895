@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
@endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Thêm thông tin ca sĩ</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
        @endif
        <form action="{{route('addSinger')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên ca sĩ:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên ca sĩ" name="name" required>
                <div class="valid-feedback">
                    Looks good!
                </div>

            </div>
            <div class="form-group">
                <label >Tên thật:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên thật ca sĩ" name="real_name" required>
            </div>
            <div class="form-group">
                <label >Ngày sinh:</label>
                <input type="date" class="form-control"  placeholder="Nhập ngày sinh ca sĩ" name="date_of_birth" required>
            </div>
            <div class="form-group">
                <label >Quốc tịch</label>
                <select name="nation" class="form-control" required>
                    <option value="">Choose...</option>
                    <option value="VietNam">VietNam</option>
                    <option value="US-UK">US-UK</option>
                    <option value="KPOP">KPOP</option>
                </select>
            </div>
            <div class="form-group">
                <label >Thông tin</label>
                <textarea class="form-control" rows="3" name="description" required></textarea>
            </div>
            <div class="form-group">
                <label>Avatar</label>
                <input type="file" class="form-control-file" name="avatar" required >
            </div>
            <div class="form-group">
                <label>Category</label>
                <select name="id_category" class="form-control" required>
                    <option value="">Choose...</option>
                    @foreach($listCategory as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Thêm ca sĩ</button>
            </div>
        </form>
    </div>
@endsection