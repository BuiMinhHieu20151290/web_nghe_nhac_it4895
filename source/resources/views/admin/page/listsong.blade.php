@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlibaihat.css')}}">
    @endsection
@section('content')
    <div id="quanlibaihat" class="d-flex" style="padding: 15px">
        <table class="table table-striped " id="danhsachbaihat" >
            <thead class="thead-dark">
            <tr style="text-align: center">
                <th  rowspan="2">STT</th>
                <th scope="col" rowspan="2">Name</th>
                <th scope="col" rowspan="2">Lyric</th>
                <th scope="col" rowspan="2">Singer</th>
                <th scope="col" rowspan="2">Composer</th>
                <th scope="col" rowspan="2">Category</th>
                <th scope="col" rowspan="2">Image</th>
                <th scope="col" rowspan="2">View</th>
                <th scope="col" rowspan="2">Like</th>
                <th scope="col" colspan="2" style="text-align: center" rowspan="1" >Actions</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center" >Delete</th>
                <th >Update</th>
            </tr>

            </thead>
            <tbody>
                @foreach($listSong as $key=>$song)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$song->name}}</td>
                        <td>{{$song->lyric}}</td>
                        <td>{{$song->singer->name}}</td>
                        <td>{{$song->composer}}</td>
                        <td>{{$song->category->name}}</td>

                        <td>
                            <img src="{{asset($song->image)}}" width="40px" height="40px" alt="Image"/>
                        </td>
                        <td>{{$song->view}}</td>
                        <td>{{$song->like}}</td>
                        <td>
                            <a  type="button" class="btn btn-primary " href="{{route('updateSong',['id'=>$song->id])}}">Sửa</a>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#myModal'.$song->id}}" >Xóa</button>
                            <div class="modal" id="{{'myModal'.$song->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Thông báo</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            Bạn có chắc chắn muốn xóa không
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            <a href="{{route('deleteSong',['id'=>$song->id])}}" class="btn btn-primary">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
        <div id="content-update-song" style="display: none">
            <button type="button" class="btn btn-outline-primary" id="back" onclick="table_display()">Quay lại</button>
            <h2 style="text-align: center">Sửa thông tin bài hát</h2>
            <form action="/action_page.php">
                <div class="form-group">
                    <label for="email">Tên bài hát:</label>
                    <input type="email" class="form-control" id="email" placeholder="Nhập tên bài hát" name="email">
                </div>
                <div class="form-group">
                    <label >Lời bài hát</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                </div>
                <div class="form-inline form-group ">
                    <label for="email" class="mr-sm-2">Ca sĩ:</label>
                    <select class="form-control" id="singer" style="width: 285px">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                    <label  class="ml-5" >Nhạc sĩ:</label>
                    <select class="form-control" id="composer" style="width: 285px">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                    <label class="ml-5 " >Thể loại:</label>
                    <select class="form-control" id="category" style="width: 285px">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Chọn file</label>
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary" style="width: 150px; font-weight: bold">Thêm bài hát</button>
                </div>
            </form>
        </div>
        <!--<div id="left">-->
        <!---->
        <!--</div>-->
        <!--<div id="right">-->

        <!--</div>-->
    </div>
    @endsection