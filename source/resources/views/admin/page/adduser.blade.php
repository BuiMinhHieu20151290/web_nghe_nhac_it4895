@extends('admin.index')
@section('content')
    <div class="content-add-song" style="padding: 15px">
        <h2 style="text-align: center">Thêm thông tin người dùng</h2>

            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $message)
                        {{$message}}
                        <br>
                    @endforeach
                </div>
                @endif
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
            @endif
        <form action="{{route('addUser')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên người dùng:</label>
                <input type="text" class="form-control" name="name" placeholder="Nhập tên người sử dụng" required>
                @if($errors->has('name'))

                    @endif
            </div>
            <div class="form-group">
                <label >Email:</label>
                <input type="email" class="form-control" placeholder="Nhập email người dùng" name="email" required>
                @if($errors->has('name'))
                    <div class="alert alert-danger">
                        @foreach($errors->get('email') as $message)
                            {{$message}}
                            <br>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Password:</label>
                <input type="password" class="form-control" placeholder="Nhập password" name="password" required>
                @if($errors->has('name'))
                    <div class="alert alert-danger">
                        @foreach($errors->get('password') as $message)
                            {{$message}}
                            <br>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Re-password:</label>
                <input type="password" class="form-control" placeholder="Nhập lại password" name="re-password" required>
                @if($errors->has('name'))
                    <div class="alert alert-danger">
                        @foreach($errors->get('re-password') as $message)
                            {{$message}}
                            <br>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Avatar:</label>
                <input type="file" class="form-control-file" name="avatar" required accept="image/*">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block" >Thêm tài khoản</button>
            </div>
        </form>
    </div>
    @endsection