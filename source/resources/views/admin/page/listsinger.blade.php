@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlibaihat.css')}}">
@endsection
@section('content')
    <div id="quanlibaihat" style="padding: 15px;" >
        <table class="table table-striped" id="danhsachbaihat" >
            <thead class="thead-dark">
            <tr style="text-align: center">
                <th  rowspan="2">STT</th>
                <th scope="col" rowspan="2" >Name</th>
                <th scope="col" rowspan="2" >Real name</th>
                <th scope="col" rowspan="2">Date of birth</th>
                <th scope="col" rowspan="2" >Nation</th>
                <th scope="col" rowspan="2">Description</th>
                <th scope="col" rowspan="2">Avatar</th>
                <th  scope="col" rowspan="2">Category</th>
                <th scope="col" colspan="2" style="text-align: center" rowspan="1" >Actions</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center" >Delete</th>
                <th >Update</th>
            </tr>
            </thead>
            <tbody>
                @foreach($listSinger as $singer)
                    <tr>
                        <td>{{$singer->id}}</td>
                        <td>{{$singer->name}}</td>
                        <td>{{$singer->real_name}}</td>
                        <td>{{$singer->date_of_birth}}</td>
                        <td>{{$singer->nation}}</td>
                        <td>{{$singer->description}}</td>
                        <td>
                            <img src="{{asset($singer->avatar)}}" width="50px" height="50px">
                        </td>
                        <td>{{$singer->id_category}}</td>
                        <td>
                            <a type="button" class="btn btn-primary " onclick="table_hidden()" href="{{route('updateSinger',['id'=>$singer->id])}}" >Sửa</a>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#myModal'.$singer->id}}" >
                                Xóa
                            </button>
                            <div class="modal" id="{{'myModal'.$singer->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Thông báo</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            Bạn có chắc chắn muốn xóa không
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            <a href="{{route('deleteSinger',['id'=>$singer->id])}}" class="btn btn-primary">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
        {{--<div class="container">--}}
            {{--<h2>Modal Example</h2>--}}
            {{--<!-- Button to Open the Modal -->--}}
            {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">--}}
                {{--Open modal--}}
            {{--</button>--}}

            {{--<!-- The Modal -->--}}
            {{--<div class="modal" id="myModal">--}}
                {{--<div class="modal-dialog">--}}
                    {{--<div class="modal-content">--}}

                        {{--<!-- Modal Header -->--}}
                        {{--<div class="modal-header">--}}
                            {{--<h4 class="modal-title">Thông báo</h4>--}}
                            {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                        {{--</div>--}}

                        {{--<!-- Modal body -->--}}
                        {{--<div class="modal-body">--}}
                            {{--Bạn có chắc chắn muốn xóa không--}}
                        {{--</div>--}}

                        {{--<!-- Modal footer -->--}}
                        {{--<div class="modal-footer">--}}
                            {{--<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>--}}
                            {{--<a href="{{route('')}}" class="btn btn-primary">Delete</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}
    </div>
@endsection
