@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
@endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Update thông tin thể loại</h2>
        <form action="{{route('updateCategory',['id'=>$category->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên thể loại:</label>
                <input type="text" class="form-control" required placeholder="Nhập tên thể loại" name="name" value="{{$category->name}}">
            </div>
            <div class="form-group">
                <label >Miêu tả:</label>
                <textarea class="form-control" rows="3" required name="description">{{$category->description}}</textarea>
            </div>
            <div class="form-group">
                <label >Image:</label>
                <input type="text" value="{{$category->image}}" disabled>
                <input type="file" class="form-control-file" accept="image/*" name="image"/>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block" >Update thể loại</button>
            </div>
        </form>
    </div>
@endsection