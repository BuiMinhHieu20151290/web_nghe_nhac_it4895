@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlibaihat.css')}}">
@endsection
@section('content')
    <div id="quanlibaihat" style="padding: 15px;" >
        <table class="table table-striped" id="danhsachbaihat" >
            <thead class="thead-dark">
            <tr style="text-align: center">
                <th  rowspan="2">STT</th>
                <th scope="col" rowspan="2" >Name</th>
                <th scope="col" rowspan="2" >Description</th>
                <th scope="col" rowspan="2">File</th>
                <th scope="col" rowspan="2" >Show</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center" >Delete</th>
                <th >Update</th>
            </tr>
            </thead>
            <tbody>
            @foreach($listSlide as $slide)
                <tr>
                    <td>{{$slide->id}}</td>
                    <td>{{$slide->name}}</td>
                    <td>{{$slide->description}}</td>
                    <td>
                        <img src="{{asset($slide->file)}}" width="100px" height="50px">
                    </td>
                    <td>{{$slide->show}}</td>
                    <td>
                        <a type="button" class="btn btn-primary " onclick="table_hidden()" href="{{route('updateSlide',['id'=>$slide->id])}}" >Sửa</a>
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#myModal'.$slide->id}}" >
                            Xóa
                        </button>
                        <div class="modal" id="{{'myModal'.$slide->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Thông báo</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Bạn có chắc chắn muốn xóa không
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        <a href="{{route('deleteSlide',['id'=>$slide->id])}}" class="btn btn-primary">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{--<div class="container">--}}
        {{--<h2>Modal Example</h2>--}}
        {{--<!-- Button to Open the Modal -->--}}
        {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">--}}
        {{--Open modal--}}
        {{--</button>--}}

        {{--<!-- The Modal -->--}}
        {{--<div class="modal" id="myModal">--}}
        {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}

        {{--<!-- Modal Header -->--}}
        {{--<div class="modal-header">--}}
        {{--<h4 class="modal-title">Thông báo</h4>--}}
        {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
        {{--</div>--}}

        {{--<!-- Modal body -->--}}
        {{--<div class="modal-body">--}}
        {{--Bạn có chắc chắn muốn xóa không--}}
        {{--</div>--}}

        {{--<!-- Modal footer -->--}}
        {{--<div class="modal-footer">--}}
        {{--<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>--}}
        {{--<a href="{{route('')}}" class="btn btn-primary">Delete</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--</div>--}}
    </div>
@endsection
