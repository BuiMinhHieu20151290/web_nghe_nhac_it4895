@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
@endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Update thông tin slide</h2>
        <form action="{{route('updateSlide',['id'=>$slide->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên slide:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên slide" name="slide_name" value="{{$slide->name}}" required>
                <div class="valid-feedback">
                    Looks good!
                </div>

            </div>
            <div class="form-group">
                <label >Mô tả slide:</label>
                <input type="text" class="form-control"  placeholder="Nhập mô tả slide" name="slide_description" value="{{$slide->description}}" required>
            </div>
            <div class="form-group">
                <label>File</label>
                {{--<input type="text" name="avatar" value="{{$singer->avatar}}" disabled>--}}
                <input type="file" class="form-control-file" name="slide_file" accept="image/*" >
            </div>
            <div class="form-group">
                <label >Show</label>
                <textarea class="form-control" rows="3" name="slide_show"  required>{{$slide->show}}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Update thông tin slide</button>
            </div>
        </form>
    </div>
@endsection