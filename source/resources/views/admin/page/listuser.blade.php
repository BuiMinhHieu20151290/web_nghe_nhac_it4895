@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlibaihat.css')}}">
@endsection
@section('content')
    <div id="quanlibaihat" class="d-flex" style="padding: 15px">
        <table class="table table-striped " id="danhsachbaihat" >
            <thead class="thead-dark">
            <tr style="text-align: center">
                <th  rowspan="2">STT</th>
                <th scope="col" rowspan="2">Name</th>
                <th scope="col" rowspan="2">Email</th>
                <th scope="col" rowspan="2">Password</th>
                <th scope="col" rowspan="2">Avatar</th>
                <th scope="col" colspan="2" style="text-align: center" rowspan="1" >Actions</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center" >Delete</th>
                <th >Update</th>
            </tr>

            </thead>
            <tbody>
                @foreach($listUser as $key=>$user)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->password}}/td>
                        <td>
                            <img src="{{asset($user->avatar)}}" width="40px" height="40px" alt="image"/>
                        </td>
                        <td>
                            <a type="button" class="btn btn-primary " href="{{route('updateUser',['id'=>$user->id])}}">Sửa</a>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#myModal'.$user->id}}" >
                                Xóa
                            </button>
                            <div class="modal" id="{{'myModal'.$user->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Thông báo</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            Bạn có chắc chắn muốn xóa không
                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            <a href="{{route('deleteUser',['id'=>$user->id])}}" class="btn btn-primary">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                        @endforeach
            </tbody>
        </table>
        <!--<div id="left">-->
        <!---->
        <!--</div>-->
        <!--<div id="right">-->

        <!--</div>-->
    </div>
@endsection