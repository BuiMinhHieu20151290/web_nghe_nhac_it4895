@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
    @endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Thêm album</h2>
        @if(Session::has('thongbao'))
            <div class="alert alert-success">{{Session::get('thongbao')}}</div>
            @endif
        <form action="{{route('addAlbum')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên album:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên album" name="name" required>
            </div>
            <div class="form-group">
                <label >Ngày phát hành</label>
                <input type="date" class="form-control" name="publish" required>
            </div>
            <div class="form-group">
                <label>Ca sĩ:</label>
                <select class="custom-select mr-sm-2" required name="singer">
                    <option value="">Choose...</option>
                    @foreach($listSinger as $singer)
                        <option value="{{$singer->id}}">{{$singer->name}}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Image</label>
                <input type="file" class="form-control-file" required name="image" accept="image/*">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block" >Thêm album</button>
            </div>
        </form>
    </div>
    @endsection
