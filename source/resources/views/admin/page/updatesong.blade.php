@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('action/css/thembaihat.css')}}">
@endsection
@section('content')
    <div class="content-add-song" style="padding: 15px">
        <h2 style="text-align: center">Sửa thông tin bài hát</h2>
        <form action="{{route('updateSong',['id'=>$song->id])}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Tên bài hát:</label>
                <input type="text" class="form-control"  placeholder="Nhập tên bài hát" name="name" value="{{$song->name}}">
            </div>
            <div class="form-group">
                <label >Lời bài hát</label>
                <textarea class="form-control" rows="5" name="lyric" required>{{$song->lyric}}</textarea>
            </div>
            <div class="form-inline form-group ">
                <label class="mr-sm-2">Ca sĩ:</label>
                <select class="form-control" name="id_singer" style="width: 285px" required>
                    <option value="">Choose...</option>
                    @foreach($listSinger as $singer)
                        @if($singer->id==$song->singer->id)
                            <option value="{{$singer->id}}" selected>{{$singer->name}}</option>
                            @else
                            <option value="{{$singer->id}}">{{$singer->name}}</option>
                            @endif
                    @endforeach
                </select>
                <label  class="ml-5" >Nhạc sĩ:</label>
                <input type="text" name="composer" class="form-control" style="width: 285px" required value="{{$song->composer}}">
                <label class="ml-5 " >Thể loại:</label>
                <select class="form-control" name="id_category" style="width: 285px" required>
                    <option value="">Choose...</option>
                    @foreach($listCategory as $category)
                        @if($category->id==$song->category->id)
                            <option value="{{$category->id}}" selected>{{$category->name}}</option>
                            @else
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endif
                    @endforeach
                </select>

            </div>
            <div class="form-group">
                <label >File:</label>
                <div>
                    <input type="text" value="{{$song->file}}" disabled>
                </div>
                <input type="file" class="form-control-file" name="file" accept="audio/mp3" />
            </div>
            <div class="form-group">
                <label >Image:</label>
                <div>
                    <input type="text" value="{{$song->image}}" disabled>
                </div>
                <input type="file" class="form-control-file" name="image" accept="image/*" >
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block" >Update bài hát</button>
            </div>
        </form>
    </div>
@endsection