@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/thembaihat.css')}}">
@endsection
@section('content')
    <div class="content-add-song">
        <h2 style="text-align: center">Chi tiết bình luận bài hát</h2>
        <form >
            @csrf
            <div class="form-group">
                <label >Người dùng:</label>
                <input type="text" class="form-control" value="{{$commentAlbum->user->name}}" readonly>
            </div>
            <div class="form-group">
                <label >Tên album:</label>
                <input type="text" class="form-control" value="{{$commentAlbum->album->name}}" readonly>
            </div>
            <div class="form-group">
                <label>Nội dung bình luận:</label>
                <textarea  class="form-control" readonly>{{$commentAlbum->content}}</textarea>
            </div>
            <div class="form-group">
                <label >Ngày bình luận:</label>
                <input type="text" class="form-control" value="{{$commentAlbum->created_at}}" readonly>
            </div>

        </form>
    </div>
@endsection
