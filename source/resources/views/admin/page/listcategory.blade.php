@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlibaihat.css')}}">
    @endsection
@section('content')
    <div id="quanlibaihat" style="padding: 15px;" >
        <table class="table table-striped" id="danhsachbaihat" >
            <thead class="thead-dark">
            <tr style="text-align: center">
                <th  rowspan="2">STT</th>
                <th scope="col" rowspan="2">Name</th>
                <th scope="col" rowspan="2">Description</th>
                <th scope="col" rowspan="2">Image</th>
                <th scope="col" colspan="2" style="text-align: center" rowspan="1" >Actions</th>
            </tr>
            <tr style="text-align: center">
                <th style="text-align: center" >Delete</th>
                <th >Update</th>
            </tr>
            </thead>
            <tbody>
                @foreach($listCategory as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->name}}</td>
                    <td>{{$category->description}}</td>
                    <td>
                        <img src="{{asset($category->image)}}" width="40px" height="40px" alt="image" />
                    </td>
                    <td>
                        <a type="button" class="btn btn-primary " href="{{route('updateCategory',['id'=>$category->id])}}">Sửa</a>
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#myModal'.$category->id}}" >
                            Xóa
                        </button>
                        <div class="modal" id="{{'myModal'.$category->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Thông báo</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        Bạn có chắc chắn muốn xóa không
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        <a href="{{route('deleteCategory',['id'=>$category->id])}}" class="btn btn-primary">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div id="content-update-song" style="display: none"  >
            <button type="button" class="btn btn-outline-primary" id="back" onclick="table_display()">Quay lại</button>
            <h2 style="text-align: center">Sửa thông tin album</h2>
            <form action="">
                <div class="form-group">
                    <label for="email">Tên album:</label>
                    <input type="email" class="form-control" id="email" placeholder="Nhập tên bài hát" name="email">
                </div>
                <div class="form-group">
                    <label >Description</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary" style="width: 150px; font-weight: bold">Thêm bài hát</button>
                </div>
            </form>
        </div>
        <!--<div id="left">-->
        <!---->
        <!--</div>-->
        <!--<div id="right">-->

        <!--</div>-->
    </div>
    @endsection