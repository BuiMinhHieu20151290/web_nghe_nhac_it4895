@extends('admin.index')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/quanlibaihat.css')}}">
    @endsection
@section('content')
    <div id="quanlibaihat" style="padding: 15px;" >
        <table class="table table-striped" id="danhsachbaihat" >
            <thead class="thead-dark">
            <tr style="text-align: center">
                <th >STT</th>
                <th >Bài hát</th>
                <th >User</th>
                <th> Content</th>
                <th>Like</th>
                <th>Dislike</th>
                <th >Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Một bước yêu vạn dặm đau</td>
                <td>Bùi Minh Hiếu</td>
                <td>Good Job</td>
                <td>100</td>
                <td>2</td>
                <td>
                    <button type="button" class="btn btn-primary " onclick="table_hidden()">Xóa</button>
                </td>
            </tr>
            </tbody>
        </table>
        <!--<div id="left">-->
        <!---->
        <!--</div>-->
        <!--<div id="right">-->

        <!--</div>-->
    </div>
    @endsection