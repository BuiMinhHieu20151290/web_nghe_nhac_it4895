<div id="playMusic" style="eight:80px;background-color: red;background-image: linear-gradient(red, yellow);" class="container-fluid">
    <div class="row">
        <div id="left" class=" col-md-1" style="background-color: #24191d;background-image: linear-gradient(red, yellow);">
            <img id="imageSmall" src="image/mv-vn.jpg" alt="Hinh Anh" width="70px" height="70px" class="rounded-circle" />
        </div>
        <div class="d-flex flex-column col-md-11" >
            <div class="mt-auto" style="display: flex; align-items: center">
                <h4 style="width: 450px;" ><span id="name_song" style="cursor: pointer" onclick="loadLink('/loadDetailSong')">Một bước yêu vạn lần đau </span> <small class="text-secondary" id="name_singer">Mr.Siro</small></h4>
                <div style="display: block"  class="btn-group list-button" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-primary " onclick="fastSong()">
                        <span  class="glyphicon glyphicon-fast-backward"></span>
                    </button>
                    <button type="button" class="btn btn-primary  " onclick="nextSong()">
                        <span class="glyphicon glyphicon-fast-forward" ></span>
                    </button>
                    <button type="button" class="btn btn-primary " onclick="loop()">
                        <span class="glyphicon glyphicon-repeat" id="btn_loop"></span>
                    </button>
                    <button type="button" class="btn btn-primary " onclick="mix_song()">
                        <span class="glyphicon glyphicon-random" id="btn_mix"></span>
                    </button>
                </div>
            </div>
            <div>
                <audio id="audio" controls autoplay>
                    <source src="" type="audio/ogg">
                    Your browser dose not Support the audio Tag
                </audio>
            </div>
        </div>
    </div>
</div>
<div class="listSong container pt-5 shadow" style="background-image: url('{{asset('user/image/music-colour.jpg')}}')">
    <div class="row">
        <div class="col-md-7">
            <div class="title-listSong">
                <h4 style="font-weight: bold; text-align: center">Danh sách phát (80)</h4>
            </div>
            <div class="content-listSong">
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5 text-secondary">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>
                <a href="#">
                    <div class="row" style="font-size: 18px">
                        <div class="col-md-1">1</div>
                        <div class="col-md-6">That Girl</div>
                        <div class="col-md-5">Olly Murs</div>
                    </div>
                </a>

            </div>
        </div>
        <div class="col-md-5 pt-5">
            <div>
                <img id="img" src="image/mv-vn.jpg" width="200px" height="200px" class="rounded-circle" style="display: block;margin: auto">
            </div>
            <div style="text-align: center;">
                <br>
                <div style="font-weight: bold;font-size: 20px;" class="text-secondary"><span class="glyphicon glyphicon-heart-empty"></span> Thích <span class="glyphicon glyphicon-comment"></span>Bình luận</div>
                <h3 style="font-weight: bold;">Một bước yêu vạn lần đau</h3>
                <h4  style="font-weight: bold;" class="text-secondary">Mr.Siro</h4>
            </div>
        </div>
    </div>
    <div id="menu-down" style="position: absolute; top:20px;font-size: 30px; font-weight: bold;right:20px;"><span id="down" class="glyphicon glyphicon-menu-down" style="cursor: pointer;"></span></div>
</div>