@foreach($listComment as $comment)
    <div class="media pt-5 d-flex align-items-center">
        <img src="{{asset('user/image/song-vn.jpg')}}" alt="John Doe" class="rounded-circle mr-3" width="50px" height="50px" >
        <div class="media-body ">
            <h4 style="margin: 0px;">{{$comment->user->name}} <small><i>Posted on {{$comment->created_at}}</i></small></h4>
            <p style="margin: 0px;">{{$comment->content}}</p>
        </div>
    </div>
@endforeach