@extends('user.index')
@section('content')
    <div class="content">
        <div class="container">
            <div class="zingchart-tuan border-dark mb-3 p-3 shadow">
                <h3>#ZINGCHART TUẦN</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div class="card">
                            <a href="zingchart.html"><img class="card-img-top" src="{{asset('user/image/song-vn.jpg')}}" alt="Card image"></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <a href="zingchart.html" ><img class="card-img-top" src="{{asset('user/image/mv-vn.jpg')}}" alt="Card image"></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <a href="zingchart.html"><img class="card-img-top" src="{{asset('user/image/song-kpop.jpg')}}" alt="Card image"></a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <a href="zingchart.html"><img class="card-img-top" src="{{asset('user/image/song-usuk.jpg')}}" alt="Card image"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top100 mb-3 p-3 shadow">
                <h3>
                    TOP 100
                </h3>
                <div id="carouselExampleIndicators-Top100" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators-Top100" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators-Top100" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators-Top100" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 30px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 30px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 30px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 30px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="top100_detail.html">
                                            <img class="card-img-top" src="{{asset('user/image/top100/audiophile.jpg')}}" alt="Card image">
                                            <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators-Top100" role="button" data-slide="prev" >
                        <span class="carousel-control-prev-icon" aria-hidden="true" ></span>
                        <span class="sr-only" >Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators-Top100" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="baihaimoi mb-3 p-3 shadow">
                <h3>Bài hát mới</h3>
                <div class="row" >
                    <div class="col-md-6" style="border-right: 1px solid #efefef;">
                        @foreach($newSong as $key=>$song)
                            @if($key<($newSong->count())/2)
                                <div class="row mb-1 item-song">
                                    <a style="cursor: pointer" onclick="loadMP3({{$song}})">
                                        <div class="col-md-2 item-song-image" >
                                            <img class="rounded" src="{{asset($song->image)}}" alt="#" width="50px" height="50px"/>
                                            <div class="play-hidden"><span class="glyphicon glyphicon-play "></span></div>
                                        </div>
                                        <div class="col-md-10 item-song-content" style="padding-left: 40px;" >
                                            <p style="font-weight: bold;" class="item-song-name">{{$song->name}}</p>
                                            <p style="margin-top: -10px" class="text-secondary item-song-artist">{{$song->singer->name}}</p>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <div class="col-md-6">
                        @foreach($newSong as $key=>$song)
                            @if($key>=($newSong->count())/2)
                                <div class="row mb-1 item-song">
                                    <a style="cursor: pointer" onclick="loadMP3({{$song}})">
                                        <div class="col-md-2 item-song-image" >
                                            <img class="rounded" src="{{asset($song->image)}}" alt="#" width="50px" height="50px"/>
                                            <div class="play-hidden"><span class="glyphicon glyphicon-play "></span></div>
                                        </div>
                                        <div class="col-md-10 item-song-content" style="padding-left: 40px;" >
                                            <p style="font-weight: bold;" class="item-song-name">{{$song->name}}</p>
                                            <p style="margin-top: -10px" class="text-secondary item-song-artist">{{$song->singer->name}}</p>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="theloai mb-3 p-3 shadow">
                <h3>
                    Album mới nhất
                </h3>
                <div class="row">
                    @foreach($newAlbum as $album)
                        <div class="col-md-3">
                            <div class="card">
                                <a onclick="">
                                    <img class="card-img-top" src="{{$album->image}}" alt="Card image">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="nghesi mb-3 p-3 shadow">
                <h3>
                    Nghệ sĩ nổi bật <span class="glyphicon glyphicon-cd"></span>
                </h3>
                <div id="carouselExampleIndicators-nghesi" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators-nghesi" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators-nghesi" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators-nghesi" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="nghesi_detail.html">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="nghesi_detail.html">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="nghesi_detail.html">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="nghesi_detail.html">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="nghesi_detail.html">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="nghesi_detail.html">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="#">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="#">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="#">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="#">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="#">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <a href="#">
                                            <img class="card-img-top rounded-circle" src="image/top100/audiophile.jpg" alt="Card image">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators-nghesi" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators-nghesi" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection