<div class="container shadow">
    <div class="jumbotron bg-dark" style="padding-top: 20px; padding-bottom: 20px;">
        <div class="row">
            <div class="col-md-3">
                <img src="{{asset($song->image)}}" alt="Hinh Anh" class="rounded" width="120px" height="120px"/>
            </div>
            <div class="col-md-9">
                <h4 style="color: white; font-weight: bold">{{$song->name}}</h4>
                <p class="text-secondary " style="font-size: 15px">Ca sĩ:{{$song->singer->name}}</p>
                <p class="text-secondary " style="font-size: 15px">Album:Một bước xa</p>
            </div>
            <div id="icon" style="position: absolute; right:100px;bottom: -460px ">
                <button style="color: white; font-weight: bold;background-color: Transparent;border: none;font-size: 18px;"><i class="far fa-play-circle"></i> {{$song->view}}</button>
                <button style="color: white; font-weight: bold;background-color: Transparent;border: none;font-size: 18px;"><i class="far fa-heart"></i> {{$song->like_song->count()}}</button>
            </div>
        </div>
    </div>
    <div class="action">
        <button type="button" class="btn btn-primary" style="font-weight: bold;border: none;font-size: 18px;" onclick="loadMP3({{$song}})"><i class="fas fa-play-circle"></i> Nghe bài hát</button>
        <button  type="button" class="btn" style="font-weight: bold;background-color: Transparent;border: none;font-size: 18px;" id="likeSong"><i class="far fa-heart"></i> Thích</button>
        <button type="button" class="btn"  style="font-weight: bold;background-color: Transparent;border: none;font-size: 18px;" id="commentSong"><i class="far fa-comment"></i> Bình luận <span class="badge badge-info" style="position: absolute; top:760px;">{{count($listComment)}}</span></button>
        <button type="button" class="btn" style="font-weight: bold;background-color: Transparent;border: none;font-size: 18px;"><i class="fas fa-cloud-download-alt"></i> Tải xuống</button>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-8">
            <div class="artist">
                <div class="d-flex align-items-center ">
                    <div>
                        <img src="{{asset($song->singer->avatar)}}" width="50px" height="50px" class="rounded-circle">
                    </div>
                    <div style="margin-left: 15px;">
                        <p style="font-weight: bold; margin: 0px;">{{$song->singer->name}}</p>
                        <p class="text-secondary" style="margin: 0px">{{$song->singer->like_singer->count()}} người theo dõi</p>
                    </div>
                    <div class="ml-auto">
                        <button type="button" class="btn btn-danger" id="likeSinger">Quan tâm <i class="far fa-star"></i></button>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="lyric" >
                <h4 class="text-dark" style="font-weight: bold">Lời bài hát</h4>
                <p style="height: 200px; overflow-y: scroll">
                    {{$song->lyric}}
                </p>
                <hr>
            </div>
            <div class="comment">
                <h4 style="font-weight: bold">{{count($listComment)}} bình luận</h4>
                <br>
                <div class="comment-action d-flex">
                    <div class="ml-3">
                        <div id="comment_empty"></div>
                        <form style="width:550px">
                            <div class="form-group" >
                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                @if(Auth::check())
                                    <textarea class="form-control" rows="2" placeholder="Nhập bình luận của bạn" id="comment_song"></textarea>
                                    <button type="button" align="right" style="float: right;" class="mt-2 btn btn-primary" onclick="comment([{{Auth::user()->id}},{{$song->id}}])">Bình luận</button>
                                    @else
                                    <textarea class="form-control" rows="2" placeholder="Bạn phải đăng nhập trước khi bình luận " disabled></textarea>
                                    <button type="button"  value="Bình luận" align="right" style="float: right;" class="mt-2 btn btn-primary">Bình luận</button>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
                @if(count($listComment)>0)
                    <div id="list_comment_song">
                        @foreach($listComment as $comment)
                            <div class="media pt-5 d-flex align-items-center">
                                <img src="{{asset('user/image/song-vn.jpg')}}" alt="John Doe" class="rounded-circle mr-3" width="50px" height="50px" >
                                <div class="media-body ">
                                    <h4 style="margin: 0px;">{{$comment->user->name}} <small><i>Posted on {{$comment->created_at}}</i></small></h4>
                                    <p style="margin: 0px;">{{$comment->content}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @else
                    <p><i>Không có bình luận cho bài hát</i></p>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <h3>Có thể bạn quan tâm</h3>
            <hr>
            @foreach($listSong as $s)
                <div class="items d-flex align-items-center p-2">
                    <a onclick="loadMP3({{$s}})" style="cursor: pointer"><img src="{{asset($s->image)}}" alt="image" width="40px" height="40px" class="rounded" /></a>
                    <div class="ml-2">
                        <a href="#">
                            <p style="margin: 0px">{{$s->name}}</p>
                            <p class="text-secondary" style="margin: 0px;">{{$s->singer->name}}</p>
                        </a>
                    </div>
                    <div class="item-hidden">
                        <a onclick="loadMP3({{$s}})"><span class="glyphicon glyphicon-play" ></span></a>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</div>
<script>
    function comment(info) {
        console.log('input');
        var content=document.getElementById('comment_song').value;
        if(content===''){
            document.getElementById('comment_empty').innerHTML="<div class='alert alert-danger'>Ban cần nhập nội dung trước khi bình luận</div>";
            setTimeout(function () {
                document.getElementById('comment_empty').innerHTML='';
            },5000);
        }else{
            console.log(info);
            document.getElementById('comment_song').value='';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var arg={
                type:'POST',
                dataType:'json',
                url:'/comment_song',
                data:{
                    idUser:info[0],
                    idSong:info[1],
                    content_comment:content
                },
                success:function (data) {
                    document.getElementById('comment_empty').innerHTML="<div class='alert alert-success'>".concat(data,"</div>");
                    setTimeout(function () {
                        document.getElementById('comment_empty').innerHTML='';
                    },5000);
                },
                error:function (error) {
                    console.log(error)
                }
            }
            $.ajax(arg);
            var arg_two= {
                type:'GET',
                dataType: 'json',
                data:{ id: info[1]},
                url:'/loadCommentSong',
                success:function (data) {
                    document.getElementById("list_comment_song").innerHTML=data;
                },
                error: function (error) {
                    console.log(error);
                }
            }
            $.ajax(arg_two);

        }

    }
    @if(Auth::check())
        checkBegin([{{Auth::user()->id}}, {{$song->id}}]);
    @endif
    function checkBegin(info){
        var buttonLike= document.getElementById('likeSong');
        var arg={
            type:'GET',
            dataType:'json',
            data:{ idUser: info[0],idSong: info[1]},
            url:'/checkLike',
            success:function (data) {
                if(data==0){
                    buttonLike.innerHTML="<i class='far fa-heart'></i> Thích";
                }else{
                    buttonLike.innerHTML="<i class='far fa-heart' style='color:red;'></i> Thích";
                }
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }
    var likeSong= document.getElementById('likeSong');
    likeSong.addEventListener('click',function () {
        @if(Auth::check())
            check([{{Auth::user()->id}},{{$song->id}}]);
            @else
        alert("Bạn cần đăng nhập trước khi thực hiện chức năng");
        @endif
    });
    function check(info) {
        var buttonLike= document.getElementById('likeSong');
        var arg={
            type:'GET',
            dataType:'json',
            data:{ idUser: info[0],idSong: info[1]},
            url:'/checkLike',
            success:function (data) {
                if(data==0){
                    likeDetailSong(info);
                    buttonLike.innerHTML="<i class='far fa-heart' style='color:red;'></i> Thích";
                }else{
                    dislikeSong(info);
                    buttonLike.innerHTML="<i class='far fa-heart'></i> Thích";
                }
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }
    function likeDetailSong(info){
        var arg={
            type:'GET',
            dataType:'json',
            data:{idUser: info[0], idSong:info[1]},
            url:'/likeSong',
            success:function (data) {
                console.log(data);
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }
    function dislikeSong(info){
        var arg={
            type:'GET',
            dataType:'json',
            data:{idUser: info[0], idSong:info[1]},
            url:'/dislikeSong',
            success:function (data) {
                console.log(data);
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }
    var buttonLikeSinger= document.getElementById('likeSinger');
    @if(Auth::check())
        checkLikeSinger([{{Auth::user()->id}},{{$song->singer->id}}]);
    @endif
    function checkLikeSinger(info){
        var arg={
            type:'GET',
            dataType:'json',
            data:{idUser: info[0], idSinger: info[1]},
            url:'/checkLikeSinger',
            success:function (data) {
                if(data==0){
                    console.log('Chua like ca si');
                }else{
                    console.log('da like ca si');
                    buttonLikeSinger.classList.remove('btn-danger');
                    buttonLikeSinger.classList.add('btn-primary');
                    buttonLikeSinger.innerHTML='Đã quan tâm <i class="far fa-star"></i>';
                }
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }

    buttonLikeSinger.addEventListener('click', function () {
        @if(Auth::check())
        checkCareSinger([{{Auth::user()->id}},{{$song->singer->id}}]);
        @else
            alert('Bạn cần đăng nhập trước khi quan tâm');
        @endif

    });
    {{--buttonLikeSinger.addEventListener('click',checkCareSinger([{{Auth::user()->id}},{{$song->singer->id}}]))--}}
    function checkCareSinger(info) {
        var arg={
            type:'GET',
            dataType:'json',
            data:{idUser: info[0], idSinger: info[1]},
            url:'/checkLikeSinger',
            success:function (data) {
                if(data==0){
                    likeSinger(info);
                    buttonLikeSinger.classList.remove('btn-danger');
                    buttonLikeSinger.classList.add('btn-primary');
                    buttonLikeSinger.innerHTML='Đã quan tâm <i class="far fa-star"></i>';
                }else{
                    console.log('da like ca si');
                    dislikeSinger(info);
                    buttonLikeSinger.classList.remove('btn-primary');
                    buttonLikeSinger.classList.add('btn-danger');
                    buttonLikeSinger.innerHTML='Quan tâm <i class="far fa-star"></i>';
                }
            },
            error:function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }
    function likeSinger(info) {
        var arg={
            type:'GET',
            dataType:'json',
            data:{idUser: info[0], idSinger: info[1]},
            url:'/likeSinger',
            success: function (data) {
                console.log(data);
            },
            error: function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }
    function dislikeSinger(info) {
        var arg={
            type:'GET',
            dataType:'json',
            data:{idUser: info[0], idSinger: info[1]},
            url:'/dislikeSinger',
            success: function (data) {
                console.log(data);
            },
            error: function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }
</script>
