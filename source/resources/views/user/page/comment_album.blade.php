@foreach($listComment as $comment)
    <div class="media border p-3" >
        <img src="{{asset('user/image/song-kpop.jpg')}}" width="40px" height="40px" alt="John Doe" class="mr-3 mt-3 rounded-circle" >
        <div class="media-body">
            <h4>{{$comment->user->name}} <small><i>Posted on {{$comment->created_at}}</i></small></h4>
            <p>{{$comment->content}}</p>
        </div>
    </div>
    @endforeach