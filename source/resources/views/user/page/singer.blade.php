<div class="content">
    <div class="container">
        @foreach($listCategory as $category)
            <div  class="mb-3 p-3 shadow">
                <h3>
                    {{$category->name}}
                </h3>
                <div class="row">
                    @foreach($category->singer as $singer)
                        <div class="col-md-3" style="margin-bottom: 20px; ">
                            <div class="card " style="padding-top: 15px">
                                <img onclick="load_detail_singer({{$singer->id}})" class="rounded-circle" src="{{asset($singer->avatar)}}" width="120px" height="120px" alt="Card image" style="margin: auto;cursor: pointer;">
                                <div class="card-body text-center">
                                    <h4 class="card-title text-dark">{{$singer->name}}</h4>
                                    <p class="card-text text-secondary"><span id="count_{{$singer->id}}">{{$singer->like_singer->count()}}</span> quan tâm </p>
                                    @if(Auth::check())
                                        @if(\App\LikeSinger::where([['id_user','=',Auth::user()->id],['id_singer','=',$singer->id]])->get()->count()>0)
                                            <button class="btn btn-primary" role="button" id="singer_{{$singer->id}}" onclick="checkLike([{{Auth::user()->id}},{{$singer->id}}])">Đã quan tâm <i class="far fa-star"></i></button>
                                        @else
                                            <button class="btn btn-danger" role="button" id="singer_{{$singer->id}}" onclick="checkLike([{{Auth::user()->id}},{{$singer->id}}])">Quan tâm <i class="far fa-star"></i></button>
                                        @endif
                                    @else
                                        <button class="btn btn-danger" role="button" id="singer_{{$singer->id}}" >Quan tâm <i class="far fa-star"></i></button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
</div>
<script>
    function load_detail_singer(id) {
        var arg={
            type:'GET',
            url:'/detail_singer',
            data:{id:id},
            dataType:'json',
            success:function (result) {
                $('div.content').html(result)
            },
            error:function (response) {
                console.log(response);
            }
        };
        $.ajax(arg);
    }
    function checkLike(info){
        var arg= {
            type: 'GET',
            dataType: 'json',
            data: {idUser: info[0], idSinger: info[1]},
            url: '/checkLikeSinger',
            success: function (data) {
                if (data == 0) {
                    likeSinger(info);
                    var count_= document.getElementById('count_'.concat(info[1])).innerText;
                    console.log(count_);
                    document.getElementById('count_'.concat(info[1])).innerHTML=parseInt(count_, 10)+1;
                    var buttonLikeSinger = document.getElementById('singer_'.concat(info[1]));
                    buttonLikeSinger.classList.remove('btn-danger');
                    buttonLikeSinger.classList.add('btn-primary');
                    buttonLikeSinger.innerHTML='Đã quan tâm <i class="far fa-star"></i>';
                } else {
                    dislikeSinger(info);
                    var count_= document.getElementById('count_'.concat(info[1])).innerText;
                    console.log(count_);
                    document.getElementById('count_'.concat(info[1])).innerHTML=parseInt(count_, 10)-1;
                    var buttonLikeSinger = document.getElementById('singer_'.concat(info[1]));
                    buttonLikeSinger.classList.remove('btn-primary');
                    buttonLikeSinger.classList.add('btn-danger');
                    buttonLikeSinger.innerHTML='Quan tâm <i class="far fa-star"></i>';
                }
            },
            error: function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }
    function likeSinger(info) {
        var arg={
            type:'GET',
            dataType:'json',
            data:{idUser: info[0], idSinger: info[1]},
            url:'/likeSinger',
            success: function (data) {
                console.log(data);
            },
            error: function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }
    function dislikeSinger(info) {
        var arg={
            type:'GET',
            dataType:'json',
            data:{idUser: info[0], idSinger: info[1]},
            url:'/dislikeSinger',
            success: function (data) {
                console.log(data);
            },
            error: function (err) {
                console.log(err);
            }
        }
        $.ajax(arg);
    }

</script>