<div class="content">
    <div class="container">
        <div class="zingchart-tuan border-dark mb-3 p-3 shadow">
            <h3 style="color: white">#ZINGCHART TUẦN</h3>
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <a href="zingchart.html"><img class="card-img-top" src="{{asset('user/image/song-vn.jpg')}}" alt="Card image"></a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <a href="zingchart.html" ><img class="card-img-top" src="{{asset('user/image/mv-vn.jpg')}}" alt="Card image"></a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <a href="zingchart.html"><img class="card-img-top" src="{{asset('user/image/song-kpop.jpg')}}" alt="Card image"></a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <a href="zingchart.html"><img class="card-img-top" src="{{asset('user/image/song-usuk.jpg')}}" alt="Card image"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="top100 mb-3 p-3 shadow">
            <h3 style="color: #c1c1c1;">
                TOP 100
            </h3>
            <div class="row">
                @foreach($listCategory as $category)
                    <div class="col-md-3" style="cursor: pointer" onclick="loadViewTop100({{$category->id}})">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{$category->image}}" style="width: 100%; height: 200px" alt="Category"/>
                            </div>
                            <div class="card-footer" style="text-decoration: none; text-align: center; font-size: 18px; color: #333">
                                Top 20 {{$category->name}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="baihaimoi mb-3 p-3 shadow">
            <h3 style="color: #c1c1c1;">Bài hát mới</h3>
            <div class="row" >
                <div class="col-md-6" style="border-right: 1px solid #efefef;">
                    @foreach($newSong as $key=>$song)
                        @if($key<($newSong->count())/2)
                            <div class="row mb-1 item-song">
                                <a style="cursor: pointer" onclick="loadMP3({{$song}})">
                                    <div class="col-md-2 item-song-image" >
                                        <img class="rounded" src="{{asset($song->image)}}" alt="#" width="50px" height="50px"/>
                                        <div class="play-hidden"><span class="glyphicon glyphicon-play "></span></div>
                                    </div>
                                    <div class="col-md-10 item-song-content" style="padding-left: 40px;" >
                                        <p style="font-weight: bold;" class="item-song-name">{{$song->name}}</p>
                                        <p style="margin-top: -10px" class="text-secondary item-song-artist">{{$song->singer->name}}</p>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
                <div class="col-md-6">
                    @foreach($newSong as $key=>$song)
                        @if($key>=($newSong->count())/2)
                            <div class="row mb-1 item-song">
                                <a style="cursor: pointer" onclick="loadMP3({{$song}})">
                                    <div class="col-md-2 item-song-image" >
                                        <img class="rounded" src="{{asset($song->image)}}" alt="#" width="50px" height="50px"/>
                                        <div class="play-hidden"><span class="glyphicon glyphicon-play "></span></div>
                                    </div>
                                    <div class="col-md-10 item-song-content" style="padding-left: 40px;" >
                                        <p style="font-weight: bold;" class="item-song-name">{{$song->name}}</p>
                                        <p style="margin-top: -10px" class="text-secondary item-song-artist">{{$song->singer->name}}</p>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="theloai mb-3 p-3 shadow">
            <h3 style="color: #c1c1c1;">
                Album mới nhất
            </h3>
            <div class="row">
                @foreach($newAlbum as $album)
                    <div class="col-md-3" style="cursor: pointer" onclick="loadGetData('/detail_Album',{{$album->id}})">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{$album->image}}" alt="Avatar" style="width: 100%;height: 200px">
                            </div>
                            <div class="card-footer" style="text-decoration: none; text-align: center; font-size: 18px; color: #333">
                                {{$category->name}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="nghesi mb-3 p-3 shadow">
            <h3 style="color: #c1c1c1;">
                Nghệ sĩ nổi bật <span class="glyphicon glyphicon-cd"></span>
            </h3>
            <div class="row">
                @foreach($listSinger as $singer)
                    <div class="col-md-3" style="cursor: pointer" onclick="load_detail_singer({{$singer->id}})">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{$singer->avatar}}" style="width: 100%; height: 200px; border-radius: 50%" alt="Avatar"/>
                            </div>
                            <div class="card-footer" style="text-decoration: none; text-align: center; font-size: 18px; color: #333">
                                {{$singer->name}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>