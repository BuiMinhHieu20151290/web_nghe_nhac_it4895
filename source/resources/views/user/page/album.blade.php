<div class="container">
    @foreach($listSinger as $singer)
        <div  class="mb-3 p-3 shadow">
            <h3>
                {{$singer->name}}
            </h3>
            <div class="row">
                @foreach($singer->album as $ab)
                    <div class="col-md-3" style="cursor: pointer" onclick="loadGetData('/detail_Album',{{$ab->id}})">
                        <div class="card">
                            <div class="card-body">
                                <img class="card-img-top" src="{{asset($ab->image)}}" style="width: 100%; height: 200px" alt="Card image">
                            </div>
                            <div class="card-footer" style="text-decoration: none; text-align: center; font-size: 18px; color: #333">
                                {{$ab->name}}
                            </div>
                        </div>
                    </div>
                    @endforeach
            </div>
        </div>
        @endforeach
</div>