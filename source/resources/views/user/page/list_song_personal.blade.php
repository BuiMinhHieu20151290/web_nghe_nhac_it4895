<div class="container" style="-webkit-box-shadow: 0 0 10px 3px #f9f9f9">
    @foreach($listSong as $key=>$song)
        <div class="row" style="cursor: pointer" >
            <div class="col-md-1">
                <h3 style="margin-left: 40px;">{{$key+1}}-</h3>
            </div>
            <div class="col-md-9 d-flex" >
                <a>
                    <div class="col-md-1 d-flex pt-1" >
                        <img src="{{asset($song->image)}}" width="50px" height="50px" alt="Hinh anh" style="display: block;"/>
                        <div class="icon-hidden-zingchart"><span class=" glyphicon glyphicon-play" style="font-size: 18px;"></span></div>
                    </div>
                </a>
                <div class="col-md-11 d-flex" >
                    <div class="info-song">
                        <a href="#"><p class="text-dark font-weight-bold pt-1">{{$song->name}}</p></a>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <p class="text-secondary" style="margin-top: 18px">{{$song->view}}  <i class="far fa-eye"></i></p>
            </div>
        </div>
    @endforeach
</div>
