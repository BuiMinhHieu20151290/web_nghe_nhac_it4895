<div class="content">
    <div class="container">
        <div class="card" style="width:100%">
            <img class="card-img-top" src="{{asset('user/image/img3.jpg')}}" alt="Card image">
            <div class="card-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1 d-flex align-items-center justify-content-start" >
                            <img src="{{asset($singer->avatar)}}" width="50px" height="50px" class="rounded-circle">
                        </div>
                        <div class="col-md-8">
                            <div>
                                <h4 style="font-weight: bold;" onclick="hello()">{{$singer->name}}</h4>
                                <p class="text-secondary">{{$singer->like_singer->count()}} người theo dõi</p>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex align-items-center justify-content-start">
                            @if(Auth::check())
                                @if(\App\LikeSinger::where([['id_user','=',Auth::user()->id],['id_singer','=',$singer->id]])->get()->count()>0)
                                    <button type="button" class="btn btn-primary" id="like_singer" onclick="checkOne([{{Auth::user()->id}},{{$singer->id}}])">Đã quan tâm <i class="far fa-star"></i></button>
                                @else
                                    <button type="button" class="btn btn-danger" id="like_singer" onclick="checkOne([{{Auth::user()->id}},{{$singer->id}}])">Quan tâm <i class="far fa-star"></i></button>
                                @endif
                            @else
                                <button type="button" class="btn btn-danger" onclick="alert('Bạn phải đăng nhập trước khi theo dõi')" id="like_singer">Quan tâm <i class="far fa-star"></i></button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="p-5 shadow mt-3 baihat">
            <h3 style="font-weight: bold;">Bài hát</h3>
            @if(count($singer->song)!=0)
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{asset($singer->song[0]->image)}}" width="240px" height="240px"/>
                    </div>
                    <div class="col-md-9 list-song" style="height: 240px; overflow-y: scroll;">
                        @foreach($singer->song as $song)
                            <div class="row list-song-item " >
                                <div class="col-md-1 d-flex align-items-center">
                                    <a onclick="loadMP3({{$song}})" style="cursor: pointer;">
                                        <img src="{{asset($song->image)}}" width="40px" height="40px" class="rounded">
                                    </a>
                                </div>
                                <div class="col-md-11">
                                    <a href="#">
                                        <h5>{{$song->name}}</h5>
                                    </a>
                                    <a href="#" style="display: block; margin-top: -10px;">
                                        {{$song->composer}}
                                    </a>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            @else
                <div>Bai hat trong</div>
            @endif
        </div>
        <div class="album p-5 shadow mt-3">
            <h3 style="font-weight: bold;">Album</h3>
            <div class="row">
                @if(count($singer->album)>0)
                    @foreach($singer->album as $album)
                        <div class="col-md-3" style="margin-bottom: 10px;">
                            <div class="card">
                                <a onclick="loadGetData('detail_Album',{{$album->id}})">
                                    <img class="card-img-top" src="{{$album->image}}" alt="Card image" width="150px" height="200px">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div>Album trong</div>
                @endif
            </div>
        </div>
        <div class="nghesi p-5 shadow mt-3">
            <h3>
                Nghệ sĩ <span class="glyphicon glyphicon-cd"></span>
            </h3>
            <div class="row">
                @foreach($singer_same as $same)
                    <div class="col-md-3" style="margin-bottom: 10px;">
                        <div class="card">
                            <a onclick="load_detail_singer({{$same->id}})">
                                <img class="card-img-top" src="{{asset($same->avatar)}}" alt="Card image" width="150px" height="200px">
                                <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</div>
<script>
    function hello() {
        alert('Hello');
    }
</script>