<div class="container">
    <div class="jumbotron-top100 bg-secondary jumbotron" style="padding-top: 20px; padding-bottom: 20px;">
        <div class="row ">
            <div class="col-md-2">
                <img src="{{asset($album->image)}}" class="rounded" width="120px" height="120px" />
            </div>
            <div class="col-md-10" style="color: white">
                <h2>{{$album->name}}</h2>
                <p>{{$album->singer->name}}</p>
            </div>
        </div>
    </div>
    <div class="content-top100">
        <div class="row">
            <div class="col-md-8">
                <div class="list-song">
                    <h3>Danh sách bài hát</h3>
                    <div class="list-song-content" style="height: 400px; overflow-y: scroll">
                        @if(count($detail_album)>0)
                            <ul class="list-group list-song-scroll" >
                                @foreach($detail_album as $key=>$detail )
                                    <li class="list-group-item" style="cursor: pointer">
                                        <a onclick="loadMP3({{$detail->song}})">
                                            <div class="row">
                                                <div class="col-md-1">{{$key+1}}</div>
                                                <div class="col-md-6">{{$detail->song->name}}</div>
                                                <div class="col-md-5">{{$detail->song->singer->name}}</div>
                                            </div>
                                        </a>
                                    </li>
                                    @endforeach
                            </ul>
                            @else
                            <p>Danh sach trong</p>
                        @endif
                    </div>
                    <div class="comment-top100">
                        <h3>{{count($album->comment_album)}} bình luận</h3>
                        <div class="comment-box" style="border-bottom: 1px solid #79716c; padding-bottom: 5px;">
                            <div class="row">
                                <div class="col-md-12" >
                                    <div id="comment_empty" ></div>
                                    <form >
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <div class="form-group" >
                                            @if(\Illuminate\Support\Facades\Auth::check())
                                                <textarea class="form-control" rows="2" placeholder="Nhập bình luận của bạn" id="comment_album" required></textarea>
                                                <button type="button"  value="Bình luận" align="right" style="float: right;" class="mt-2 btn btn-primary" onclick="comment([{{\Illuminate\Support\Facades\Auth::user()->id}},{{$album->id}}])" ><i class="fa fa-pencil-square-o"></i> Bình luận</button>
                                                @else
                                                <textarea class="form-control" rows="2" placeholder="Bạn cần đăng nhập trước khi bình luận của bạn" disabled ></textarea>
                                                <button type="button"  value="Bình luận" align="right" style="float: right;" class="mt-2 btn btn-primary"><i class="fa fa-pencil-square-o"></i> Bình luận</button>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="list_comment_album">
                        @if(count($album->comment_album)>0)
                            @foreach($album->comment_album as $comment)
                                <div class="media border p-3" >
                                    <img src="{{asset('user/image/song-kpop.jpg')}}" width="40px" height="40px" alt="John Doe" class="mr-3 mt-3 rounded-circle" >
                                    <div class="media-body">
                                        <h4>{{$comment->user->name}} <small><i>Posted on {{$comment->created_at}}</i></small></h4>
                                        <p>{{$comment->content}}</p>
                                    </div>
                                </div>
                                @endforeach
                            @else
                            <i>Không có nội dụng bình luận</i>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row" style="margin-top: 5px; margin-bottom: 10px;">
                    <h3>Có thể bạn quan tâm</h3>
                </div>
                <div class="info-care">
                    @foreach($list_album as $al)
                    <a onclick="loadGetData('detail_Album',{{$al->id}})" style="cursor: pointer">
                        <div class="row mb-4" >
                            <div class="col-md-2">
                                <img src="{{asset($al->image)}}" class="rounded" width="40px" height="40px">
                            </div>
                            <div class="col-md-10">
                                <p class="font-weight-bold">{{$al->name}}</p>
                            </div>
                        </div>
                    </a>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function comment(info) {
        console.log('input');
        var content=document.getElementById('comment_album').value;
        if(content==''){
            document.getElementById('comment_empty').innerHTML="<div class='alert alert-danger'>Ban cần nhập nội dung trước khi bình luận</div>";
            setTimeout(function () {
                document.getElementById('comment_empty').innerHTML='';
            },5000);
        }else{
            console.log(info);
            document.getElementById('comment_album').value='';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var arg={
                type:'POST',
                dataType:'json',
                url:'/comment_album',
                data:{
                    idUser:info[0],
                    idAlbum:info[1],
                    content_comment:content
                },
                success:function (data) {
                    document.getElementById('comment_empty').innerHTML="<div class='alert alert-success'>".concat(data,"</div>");
                    setTimeout(function () {
                        document.getElementById('comment_empty').innerHTML='';
                    },5000);
                },
                error:function (error) {
                    console.log(error)
                }
            }
            $.ajax(arg);
            var arg_two= {
                type:'GET',
                dataType: 'json',
                data:{ id: info[1]},
                url:'/loadCommentAlbum',
                success:function (data) {
                    document.getElementById("list_comment_album").innerHTML=data;
                },
                error: function (error) {
                    console.log(error);
                }
            }
            $.ajax(arg_two);
        }

    }
</script>