<div class="container">
    <div  class="mb-3 p-3 shadow">
        <h3>
            Việt Nam
        </h3>
        <div id="album1" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-Top100" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators-Top100" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-Top100" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#album1" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#album1" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div  class="mb-3 p-3 shadow">
        <h3>
            Âu mĩ
        </h3>
        <div id="album2" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-Top100" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators-Top100" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-Top100" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#album2" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#album2" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div  class="mb-3 p-3 shadow">
        <h3>
            Hòa tấu
        </h3>
        <div id="album3" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators-Top100" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators-Top100" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators-Top100" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="#">
                                    <img class="card-img-top" src="image/top100/audiophile.jpg" alt="Card image">
                                    <div class="top-item-hidden"><span class="glyphicon glyphicon-play" style="font-size: 45px;"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#album3" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#album3" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>