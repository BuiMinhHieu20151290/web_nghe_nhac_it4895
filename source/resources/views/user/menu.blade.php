<div class="menu" >
    <nav class="navbar navbar-expand-sm navbar-light  pl-200" >
        <a href="{{route('page')}}" class="navbar-brand">
            ZINGMP3
        </a>
        <form action="#" class="form-inline w-50 ">
            <input type="text" class="form-control w-100 fas"  placeholder=" &#xf002; Nhập tên ca sĩ bạn muốn tìm kiếm">
        </form>
        <div class="dropdown" style="margin-left: 100px;">
            @if(Auth::check())
                <button type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" >
                    {{Auth::user()->name}} </button>
                <div class="dropdown-menu">
                    <a href="{{route('logout')}}" class="dropdown-item">Logout</a>
                </div>
                @else
                <a href="{{route('login')}}" style="text-decoration: none; color: #5b96ff;margin-right: 10px;">Đăng nhập</a>
                <a href="{{route('register')}}" style="text-decoration: none; color: #c51f1a">Đăng kí</a>
            @endif

        </div>
    </nav>
    <nav class=" navbar navbar-expand-sm navbar-light bg-light justify-content-center" style="background-color: #1c1c1c;">
        <ul class="navbar-nav">
            <li class="nav-item active-navbar" >
                <a class="nav-link"  id="home">TRANG CHỦ</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" id="top100">#TOP 20</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="zingchart">ZING CHART</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="album">ALBUM</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="composer">NGHỆ SĨ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="music_personal">NHẠC CÁ NHÂN</a>
            </li>
        </ul>
    </nav>
</div>
<script>
    var listNavbar=document.getElementsByClassName('nav-item');
    for(var i=0;i<listNavbar.length;i++){
        listNavbar[i].addEventListener('click',function () {
            removeClass();
            this.classList.add('active-navbar');
        })
    }
    function removeClass() {
        for(var i=0;i<listNavbar.length;i++){
            listNavbar[i].classList.remove('active-navbar');
        }
    }
</script>