<!doctype html>
<html lang="en">
<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('user/css/index.css')}}" >
    <link rel="stylesheet" href="{{asset('user/css/baihat.css')}}">
    <link rel="stylesheet" href="{{asset('user/css/zingchat.css')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<style>
    *{
        font-family: sans-serif;
    }
    div#playMusic{
        display: none;
    }

    .active-navbar{
        border-bottom: 2px solid #dd8c16;
        color: #dd8c16;
    }
    li.nav-item a{
        font-size: 15px;
        font-weight: bold;
        color: #f9f9f9;
    }

</style>
<body >
    @include('user.menu')
    <div class="slider">
        <div class="container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="{{asset('user/image/img1.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('user/image/img2.jpg')}}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('user/image/img3.jpg')}}" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('user/image/img4.jpg')}}" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('user/image/img5.jpg')}}" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    @section('content')
        <div class="content">
            <div class="container">
                <div class="zingchart-tuan border-dark mb-3 p-3 shadow">
                    <h3>#ZINGCHART TUẦN</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <a href="zingchart.html"><img class="card-img-top" src="{{asset('user/image/song-vn.jpg')}}" alt="Card image"></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="zingchart.html" ><img class="card-img-top" src="{{asset('user/image/mv-vn.jpg')}}" alt="Card image"></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="zingchart.html"><img class="card-img-top" src="{{asset('user/image/song-kpop.jpg')}}" alt="Card image"></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <a href="zingchart.html"><img class="card-img-top" src="{{asset('user/image/song-usuk.jpg')}}" alt="Card image"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top100 mb-3 p-3 shadow">
                    <h3>
                        TOP 100
                    </h3>
                    <div class="row">
                        @foreach($listCategory as $category)
                            <div class="col-md-3" style="cursor: pointer" onclick="loadViewTop100({{$category->id}});">
                                <div class="card">
                                    <div class="card-body">
                                        <img src="{{$category->image}}" style="width: 100%; height: 200px" alt="Category"/>
                                    </div>
                                    <div class="card-footer" style="text-decoration: none; text-align: center; font-size: 18px; color: #333">
                                        Top 20 {{$category->name}}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                    </div>
                </div>
                <div class="baihaimoi mb-3 p-3 shadow">
                    <h3>Bài hát mới</h3>
                    <div class="row" >
                        <div class="col-md-6" style="border-right: 1px solid #efefef;">
                            @foreach($newSong as $key=>$song)
                                @if($key<($newSong->count())/2)
                                    <div class="row mb-1 item-song">
                                        <a style="cursor: pointer" onclick="loadMP3({{$song}})">
                                            <div class="col-md-2 item-song-image" >
                                                <img class="rounded" src="{{asset($song->image)}}" alt="#" width="50px" height="50px"/>
                                                <div class="play-hidden"><span class="glyphicon glyphicon-play "></span></div>
                                            </div>
                                            <div class="col-md-10 item-song-content" style="padding-left: 40px;" >
                                                <p style="font-weight: bold;" class="item-song-name">{{$song->name}}</p>
                                                <p style="margin-top: -10px" class="text-secondary item-song-artist">{{$song->singer->name}}</p>
                                            </div>
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="col-md-6">
                            @foreach($newSong as $key=>$song)
                                @if($key>=($newSong->count())/2)
                                    <div class="row mb-1 item-song">
                                        <a style="cursor: pointer" onclick="loadMP3({{$song}})">
                                            <div class="col-md-2 item-song-image" >
                                                <img class="rounded" src="{{asset($song->image)}}" alt="#" width="50px" height="50px"/>
                                                <div class="play-hidden"><span class="glyphicon glyphicon-play "></span></div>
                                            </div>
                                            <div class="col-md-10 item-song-content" style="padding-left: 40px;" >
                                                <p style="font-weight: bold;" class="item-song-name">{{$song->name}}</p>
                                                <p style="margin-top: -10px" class="text-secondary item-song-artist">{{$song->singer->name}}</p>
                                            </div>
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="theloai mb-3 p-3 shadow">
                    <h3>
                        Album mới nhất
                    </h3>
                    <div class="row">
                        @foreach($newAlbum as $album)
                            <div class="col-md-3" style="cursor: pointer" onclick="loadGetData('/detail_Album',{{$album->id}})">
                                <div class="card">
                                    <div class="card-body">
                                        <img src="{{$album->image}}" alt="Avatar" style="width: 100%;height: 200px">
                                    </div>
                                    <div class="card-footer" style="text-decoration: none; text-align: center; font-size: 18px; color: #333">
                                        {{$category->name}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="nghesi mb-3 p-3 shadow">
                    <h3>
                        Nghệ sĩ nổi bật <span class="glyphicon glyphicon-cd"></span>
                    </h3>
                    <div class="row">
                        @foreach($listSinger as $singer)
                            <div class="col-md-3" style="cursor: pointer" onclick="load_detail_singer({{$singer->id}})">
                                <div class="card">
                                    <div class="card-body">
                                        <img src="{{$singer->avatar}}" style="width: 100%; height: 200px; border-radius: 50%" alt="Avatar"/>
                                    </div>
                                    <div class="card-footer" style="text-decoration: none; text-align: center; font-size: 18px; color: #333">
                                        {{$singer->name}}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
        @show
    @include('composer.playmusic')
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="{{asset('user/js/music.js')}}"></script>
<script src="{{asset('user/js/menudown.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $('#zingchart').click(function (e) {
           e.preventDefault();
           loadLink('/zingchart');
        });
        $('#home').click(function (e) {
            e.preventDefault();
            // alert("Hello");
            loadLink('/home_zing');
        });
        $('#top100').click(function (e) {
            e.preventDefault();
            loadLink('/top100');
        });
        $('#album').click(function (e) {
            e.preventDefault();
            loadLink('/album');
        });
        $('#composer').click(function (e) {
            e.preventDefault();
            loadLink('/singer_user');
        });
        $('#music_personal').click(function (e) {
            e.preventDefault();
            loadLink('/show');
        })
    })
    function loadLink(url){
        var arg={
            url:url,
            dataType:'json',
            type:'GET',
            success:function (result) {
                $('div.content').html(result);
            }
        };
        $.ajax(arg);
    }
    function loadGetData(url, id){
        var arg={
            url:url,
            dataType: "json",
            type:'GET',
            data:{id:id},
            success:function (result) {
                $('div.content').html(result);
            },
            error:function (er) {
                console.log(er);
            }
        };
        $.ajax(arg);
    }
    var listSong=[];
    var nowSong;
    function loadMP3(info_mp3){
        document.getElementById('playMusic').style.display='block';
        var audio= document.getElementById('audio');
        var source=document.getElementById('audioSource');
        var src='http://localhost:8000/'.concat(info_mp3.file);
        var imageSmall= document.getElementById('imageSmall');
        var nameSinger= document.getElementById('name_singer');
        var nameSong= document.getElementById('name_song');
        console.log(nameSong);
        // nameSinger.content=info_mp3.singer.name;
        nameSong.textContent=info_mp3.name;
        console.log(info_mp3);
        imageSmall.src='http://localhost:8000/'.concat(info_mp3.image);
        nowSong=info_mp3;
        var check= listSong.some(song=> song.id===info_mp3.id
        );
        console.log(check);
        if(!check){
            listSong.push(info_mp3);
        }
        console.log(listSong);
        audio.src=src;
        audio.load();
        audio.play();
        var arg={
            type:'GET',
            data:{id:info_mp3.id},
            dataType:'json',
            url:'increaseViewSong',
            success:function (data) {
            },
            error:function (error) {
                console.log(error);
            }
        };
        $.ajax(arg);
        var arg_one={
            type:'GET',
            data:{id:info_mp3},
            dataType:'json',
            url:'music_personal',
            success:function (data){
                console.log(data);
            },
            error: function (error) {
                console.log(error);
            }
        }
        $.ajax(arg_one);
    }
    function nextSong() {
        if(listSong.length>1){
            var index= listSong.findIndex(song=> song.id===nowSong.id);
            console.log('')
            if(index===(listSong.length-1)){
                loadMP3(listSong[0]);
            }else{
                loadMP3(listSong[index+1]);
            }
        }
    }
    function fastSong() {
        if(listSong.length>1){
            var index= listSong.findIndex(song=> song.id===nowSong.id);
            if(index===0){
                loadMP3(listSong[listSong.length-1]);
            }else{
                loadMP3(listSong[index-1]);
            }
        }
    }
    var audio= document.getElementById('audio');
    audio.onended= function () {
        console.log('Ket thuc');
    }
    var btn_loop= document.getElementById('btn_loop');
    var btn_mix=document.getElementById('btn_mix');
    function loop() {
        var result= audio.loop;
        if(result===true){
            audio.loop=false;
            btn_loop.style.color='white';
            // btn_mix.classList.remove('btn-primary');
            // btn_mix.classList.add('btn-danger');
        }else{
            audio.loop=true;
            btn_loop.style.color='red';
            btn_mix.style.color='white';
            // btn_mix.classList.add('btn-primary');
            // btn_mix.classList.remove('btn-danger');
        }
    }
    var mix= false;
    function mix_song(){
        mix= !mix;
        if(mix==true){
            audio.loop=false;
            btn_loop.style.color='white';
            btn_mix.style.color='red';
        }else{
            btn_mix.style.color='white';
        }
    }
    audio.onended=function () {
        if(mix==true){
            nextSong();
        }
    }
    function loadViewTop100(id){
        var arg={
            url:'/top100',
            dataType:'json',
            type:'GET',
            success:function (result) {
                $('div.content').html(result);
                var arg={
                    type:'GET',
                    data:{id: id},
                    dataType:'json',
                    url:'top100_item',
                    success:function (data) {
                        document.getElementById('body-top100').innerHTML=data;
                    },
                    error:function(err){
                        console.log(err);
                    }
                }
                $.ajax(arg);
            }
        };
        $.ajax(arg);
    }
    function load_detail_singer(id) {
        var arg={
            type:'GET',
            url:'/detail_singer',
            data:{id:id},
            dataType:'json',
            success:function (result) {
                $('div.content').html(result)
            },
            error:function (response) {
                console.log(response);
            }
        };
        $.ajax(arg);
    }

</script>
    <script >

        function checkOne(info) {
            var buttonLikeSinger= document.getElementById('like_singer');
            var arg={
                type:'GET',
                dataType:'json',
                data:{idUser: info[0], idSinger: info[1]},
                url:'/checkLikeSinger',
                success:function (data) {
                    if(data==0){
                        likeSinger(info);
                        buttonLikeSinger.classList.remove('btn-danger');
                        buttonLikeSinger.classList.add('btn-primary');
                        buttonLikeSinger.innerHTML='Đã quan tâm <i class="far fa-star"></i>';
                    }else{
                        console.log('da like ca si');
                        dislikeSinger(info);
                        buttonLikeSinger.classList.remove('btn-primary');
                        buttonLikeSinger.classList.add('btn-danger');
                        buttonLikeSinger.innerHTML='Quan tâm <i class="far fa-star"></i>';
                    }
                },
                error:function (err) {
                    console.log(err);
                }
            }
            $.ajax(arg);
        }
        function likeSinger(info) {
            var arg={
                type:'GET',
                dataType:'json',
                data:{idUser: info[0], idSinger: info[1]},
                url:'/likeSinger',
                success: function (data) {
                    console.log(data);
                },
                error: function (err) {
                    console.log(err);
                }
            }
            $.ajax(arg);
        }
        function dislikeSinger(info) {
            var arg={
                type:'GET',
                dataType:'json',
                data:{idUser: info[0], idSinger: info[1]},
                url:'/dislikeSinger',
                success: function (data) {
                    console.log(data);
                },
                error: function (err) {
                    console.log(err);
                }
            }
            $.ajax(arg);
        }
        function load_detail_singer(id) {
            var arg={
                type:'GET',
                url:'/detail_singer',
                data:{id:id},
                dataType:'json',
                success:function (result) {
                    $('div.content').html(result)
                },
                error:function (response) {
                    console.log(response);
                }
            };
            $.ajax(arg);
        }
    </script>
</body>
</html>