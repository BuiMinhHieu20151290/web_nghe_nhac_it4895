<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lyric');
            $table->string('composer');
            $table->unsignedInteger('id_singer');
            $table->foreign('id_singer')->references('id')->on('singer');
            $table->unsignedInteger('id_category');
            $table->foreign('id_category')->references('id')->on('category');
            $table->integer('like');
            $table->integer('view');
            $table->string('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('song');
    }
}
