<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailAlbumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_album', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_album');
            $table->foreign('id_album')->references('id')->on('album');
            $table->unsignedInteger('id_song');
            $table->foreign('id_song')->references('id')->on('song');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_album');
    }
}
